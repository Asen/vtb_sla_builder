﻿/* 
*
*	|=========================================================================|
*	|============= Процедура сбора промежуточных показателей SLA =============|
*	|=========================================================================|
*	
*
*	Процедура либо успешно отрабатывает для ВСЕГО интервала времени для ВСЕХ sla,
*	либо, в случае ошибки, откатывается к исходному состоянию БД, таким образом
*	не отрабатывая ни для одного sla, ни для какого интервала.
*
*
*	Режимы работы:
*	
*		1)Штатный (без параметров):
*			Автоматический вызов процедуры из Job`а без прямого участия человека.
*			Сбор/Пересбор показателей SLA либо только за последние сутки, либо на интервале
*			[ последний_прогон_для_конкретного_sla, сегодня_00:00:00 )
*			
*		2)Пользовательский (с параметрами):
*			Процедуру вызывает человек вручную на интересующем отрезке дат.
*			Сбор/Пересбор показателей SLA на отрезке
*			[ G_INTERVAL_START, G_INTERVAL_END ]
*


	*** Рекомендации по использованию ***
	
		1) Так как процедура транзакционная(либо все, либо ничего), 
		   то большой интервал(более 1 месяца)
		   лучше разбивать на несколько более мелких подинтервалов, и запускать процедуру
		   отдельно для каждого подинтервала в целях локализации возможных ошибок.
		   
		2) Процедура оптимизирована для сбора данных на интервале, а не на точке.
		   Это значит, что фрагментировать исходный интервал на слишком короткие интервалы 
		   и запускать процедуру на них последовательно - неэффективно и небезопасно.
		   Запросы оптимизированы(группировка по точкам дат), а DBMS лог может переполниться,
		   спровоцировав переполнение буфера. Оптимальный интервал - от недели до месяца.
		   
		3) В случае появления нового SLA-отчета оптимально будет не вызывать всю процедуру,
		   пересобирая тем самым из-за одного нового SLA все уже собранные SLA,
		   а подредактировать курсор "SlaInfoCur" так, чтобы он возвращал только ID 
		   нового SLA-отчета и собирал данные только для него одного.

*/

CREATE OR REPLACE PROCEDURE DPC_VTB_SlaCollectData
(G_INTERVAL_START DATE DEFAULT NULL, G_INTERVAL_END DATE DEFAULT NULL) 
IS

	----------------------------------------------------------------

	-- Секция глобальных данных
	--Now        CONSTANT DATE := to_date('05/04/2016 00:09:00', 'dd/mm/yyyy hh24:mi:ss'); --debug
	Now   CONSTANT DATE   :=   sysdate; -- время запуска процедуры(Job`а)	
	CURSOR SlaInfoCur IS                -- курсор, возвращающий ID SLA, участвующих в сборе статистики
		SELECT sla_id FROM VTB_STAT_SLA_INFO 
		WHERE 1=1
		ORDER BY sla_id;
	SlaInfoCur_id     VTB_STAT_SLA_INFO.sla_id%TYPE;
	
	----------------------------------------------------------------

	
	-- Функция, возвращающая дату сбора последней локальной SLA-статистики, меньшей текущего времени,
	-- для заданного идентификатора(типа) SLA-отчета
	FUNCTION SelectLastDateBySlaId(p_sla_id IN VTB_STAT_SLA_INFO.sla_id%TYPE) RETURN DATE
	AS
		v_last_date DATE;
	BEGIN
		SELECT MAX(date_hh24) INTO v_last_date FROM VTB_STAT_SLA_DATA 
		WHERE sla_id = p_sla_id AND date_hh24 <= Now;
		RETURN v_last_date;
	END SelectLastDateBySlaId;
	
	
	-- Функция, определ¤ющая "верхнюю" границу интервала, дл¤ подготовки к 
	-- сбору локальных показателей каждого отдельно взятого SLA
	FUNCTION CalcIntervalUpperBound(p_sla_id IN VTB_STAT_SLA_INFO.sla_id%TYPE) RETURN DATE
	AS
	BEGIN
		-- режим выполнения процедуры на временном отрезке [G_INTERVAL_START, G_INTERVAL_END]
		IF G_INTERVAL_END IS NOT NULL THEN
			RETURN trunc(G_INTERVAL_END) + 1; -- включение верхней границы интервала
		-- штатный режим выполнени¤ процедуры: без параметров - из Job`а
		ELSE
			RETURN trunc(Now);
		END IF;
	END;
	
	
	-- Функция, определ¤ющая "нижнюю" границу интервала, дл¤ подготовки к 
	-- сборке локальной статистики каждого отдельно вз¤того SLA
	FUNCTION CalcIntervalLowerBound(p_sla_id IN VTB_STAT_SLA_INFO.sla_id%TYPE) RETURN DATE
	AS
		v_lower_bound DATE;
	BEGIN
		-- режим выполнения процедуры на временном отрезке [G_INTERVAL_START, G_INTERVAL_END]
		IF G_INTERVAL_START IS NOT NULL THEN
			RETURN trunc(G_INTERVAL_START);
		-- штатный режим выполнения процедуры без параметров - из Job`а
		ELSE
			v_lower_bound := SelectLastDateBySlaId(p_sla_id);
			-- cлучай, когда статистика еще ни разу не собиралась прежде по данному показателю SLA
			IF v_lower_bound IS NULL THEN			
				v_lower_bound := trunc(Now) - 1; 		
			ELSE		
				-- (+1) так как возвращенная MAX дата из таблицы - 
				-- дата, за которое произошел последний сбор локального SLA
				v_lower_bound := v_lower_bound + 1;
				-- если Job запускался более 1 раза в течение суток(lower_bound = trunc(Now)), то
				-- данные по SLA будут пересобраны заново: возврат к исходному lower_bound 
				IF (v_lower_bound=trunc(Now)) THEN 
					v_lower_bound := v_lower_bound - 1;
				END IF;					
			END IF;
			RETURN trunc(v_lower_bound);
		END IF;
	END; 
	
	
	-- Процедура логгирования хода выполнения процедуры сбора статистики SLA
	PROCEDURE WriteLog(p_id IN VTB_STAT_SLA_INFO.sla_id%TYPE, p_msg IN varchar2) 
	AS
	BEGIN
		dbms_output.put_line('-------------------- ' || to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss') || '-------------------- ');
		dbms_output.put_line('Идентификатор SLA: ' || to_char(p_id));
		dbms_output.put_line('Cообщение: ' || p_msg);
		
		-------------------------------- Debug info --------------------------------
		-- Динамическая отладка, сохраняющая дебаг-сообщения в дебаг-таблицу,
		-- которая разворачивается с помощью DeployDbgTable.sql
		--INSERT INTO VTB_STAT_SLA_DEBUG(id, dbg_msg) VALUES(p_id, p_msg);
		--COMMIT;
		---------------------------------------------------------------------------
		
	END;
	
	
	-- Главная процедура сбора локальной статистикии SLA по указанной метрике.
	-- Процедура формализована из расчета, что КАЖДЫЙ запрос возвращает ДАТУ и ПОКАЗАТЕЛЬ SLA по этой дате
	-- На вход принимается идентификатор метрики SLA из таблицы VTB_STAT_SLA_INFO
	PROCEDURE CollectData(P_SLA_ID IN VTB_STAT_SLA_INFO.sla_id%TYPE)
	AS
		v_intr_start DATE := CalcIntervalLowerBound(P_SLA_ID);  -- начало интервала сбора статистики +24h
		v_intr_end   DATE := CalcIntervalUpperBound(P_SLA_ID);  -- конец интервала сбора статистики +24h
	BEGIN
	
		-- чистка дубликатов на интервале дат в целях соблюдения целостности(CONSTRAINT uniq_data) 
		-- и непровоцирование exception`ов, если если вдруг дубликаты имеются
		DELETE FROM VTB_STAT_SLA_DATA 
		WHERE sla_id = P_SLA_ID AND date_hh24 >= v_intr_start AND date_hh24 < v_intr_end;
  
		--------------------------------- Отладка ---------------------------------	
		WriteLog(P_SLA_ID, 'Начинается сборка за интервал: ' || 
						   to_char(v_intr_start, 'dd.mm.yyyy hh24:mi:ss') || ' - ' || 
						   to_char(v_intr_end, 'dd.mm.yyyy hh24:mi:ss')
						   );
		---------------------------------------------------------------------------	
  
		-- сбор статистики по константному идентификатору SLA-метрики:
		CASE P_SLA_ID
			
			WHEN 010 THEN BEGIN -- скорость загрузки документов МЦОИ
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(DTI1)                  as RDate, 
					--avg((DTI2-DTI1)*24*60) as "ср время в мин", 
					--avg(cnt) as "ср число док",
					avg(cnt/((DTI2-DTI1)*24*60)) as Measure,
					P_SLA_ID                     as SlaReportId
				FROM(
					SELECT 
						to_date(to_char(dt1.initdate, 'YYYYMMDD HH24:MI'),'YYYYMMDD HH24:MI') as dti1, 
						max(dv.visadate) as dti2, 
						count(dt2.classified) as cnt
					FROM 
						doctree dt1, 
						doctree dt2, 
						docvisa dv 
					WHERE 
						dt1.doctype = 1000237136 -- входное сообщение R-макета
						and dt2.parent=dt1.classified
						and dt1.label like ('%PacketEPD%')
						and dt1.operdate >= v_intr_start 
						and dt1.operdate <  v_intr_end
						and dt2.classified=dv.doc
						and dv.entitystate=1000161096 -- состояние "контроль"
						and dv.userid='DAEMON'
					GROUP BY to_char(dt1.initdate, 'YYYYMMDD HH24:MI')
				) GH 
				GROUP BY trunc(DTI1);
				--ORDER BY 1;
			END;
			
			
			WHEN 020 THEN BEGIN -- скорость загрузки входящих документов SWIFT
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(to_date(dti,'YYYYMMDD HH24:MI')) as RDate, 
					max(cnt/tim)                           as Measure,
					P_SLA_ID                               as SlaReportId
				FROM(
					SELECT 
						to_char(iii.initdate, 'YYYYMMDD HH24:MI') as dti, 
						(max(iii.linkdocdate)-min(iii.initdate))*24*60*60  as tim, 
						count(IIi.classified) cnt
					FROM 
						infomessage_in iii, 
						transfermessage tmm
					WHERE 
						tmm.classified=iii.transfermessage
						and iii.clearinghouse in (1000745728,1013262899, 1000005026, 1000012590) --BRANCH, BRANCHRUR, SWIFT,SWIFT-RUR
						and iii.initdate >= v_intr_start 
						and iii.initdate <  v_intr_end
						and tmm.label not in ('MT940','MT950','MT298','MT395','MT399','MT599','MT608','MT699','MT700','MT701','MT707','MT730','MT742','MT756', 'MT760', 'MT767','MT799' ) --MT940, MT950
						-- and dt.classified=1045370651
					GROUP BY to_char(iii.initdate, 'YYYYMMDD HH24:MI') 
				) HJ
				WHERE  
					tim>0 and cnt>100
				GROUP BY trunc(to_date(dti,'YYYYMMDD HH24:MI'))  ;
				--ORDER BY 1;        
			END;
			
			
			WHEN 030 THEN BEGIN -- скорость загрузки  входящих документов TELEX
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					to_date(tlx.dDate,'dd.mm.yyyy') as RDate, 
					round(1/avg(tlx.tTime),2)       as Measure,
					P_SLA_ID                        as SlaReportId
				FROM (
					SELECT 
						to_char(trunc(operdate,'hh24'),'dd.mm.yyyy') dDate, 
						nvl(round(avg(operdate-initdate)*60*60*24,3),0) tTime
					FROM doctree
					WHERE 
						doctype in (1000668334, 1005424858) -- TELEX
						and operdate >= v_intr_start
						and operdate <  v_intr_end
					GROUP BY trunc(operdate,'hh24')
					HAVING nvl(round(avg(operdate-initdate)*60*60*24,3),0)>0
				) tlx
				GROUP BY tlx.dDate;
				--ORDER BY 1;
			END;
			
			
			WHEN 040 THEN BEGIN -- время создания/подписания расчетных документов(Создание 1 рука)
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT /*+ parallel(10) */ 
					to_date(vih1.visadate,'dd/mm/yy')                              as RDate, 
					round(avg(trunc((vih1c.visadate - vih1.visadate)*24*60*60)),2) as Measure,
					P_SLA_ID                                                       as SlaReportId
				FROM 
					docvisa vih1, 
					docvisa vih1c, 
					vtb_commreference1 cr, 
					vtb_wps wp
				WHERE 
					vih1c.doc = vih1.doc
					and wp.doc = vih1c.doc
					and vih1.visadate  >=  v_intr_start
					and vih1c.visadate >=  v_intr_start
					and vih1.visadate  <   v_intr_end
					and vih1c.visadate <   v_intr_end
					and vih1.entitymessage  = 1005370069
					and vih1c.entitymessage = 1005359845
					and cr.ctg = 83 
					and cr.n1 = (CASE 
									WHEN trunc((vih1c.visadate - vih1.visadate)*24*60*60) <= 31 THEN 
										trunc((vih1c.visadate - vih1.visadate)*24*60*60)
									ELSE 32 
								END)
				GROUP BY to_date(vih1.visadate,'dd/mm/yy');
				--ORDER BY 1;
			END;
			
			
			WHEN 050 THEN BEGIN -- время создания/подписания расчетных документов(Подписание 2 рука)
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT /*+ parallel(10) */ 
					to_date(vih1.visadate,'dd/mm/yy')                              as RDate, 
					round(avg(trunc((vih1c.visadate - vih1.visadate)*24*60*60)),2) as Measure,
					P_SLA_ID                                                       as SlaReportId
				FROM 
					docvisa vih1, 
					docvisa vih1c, 
					vtb_commreference1 cr, 
					vtb_wps wp
				WHERE 
					vih1c.doc = vih1.doc
					and wp.doc = vih1c.doc
					and vih1.visadate  >=  v_intr_start
					and vih1c.visadate >=  v_intr_start
					and vih1.visadate  <   v_intr_end
					and vih1c.visadate <   v_intr_end
					and vih1.entitymessage  = 1005359849
					and vih1c.entitymessage = 1009839471
					and cr.ctg = 83 
					and cr.n1 = (CASE 
									WHEN trunc((vih1c.visadate - vih1.visadate)*24*60*60) <= 31 THEN 
										trunc((vih1c.visadate - vih1.visadate)*24*60*60)
									ELSE 32 
								 END)
				GROUP BY to_date(vih1.visadate,'dd/mm/yy');
				--ORDER BY 1;
			END;
			
			
			WHEN 060 THEN BEGIN -- Время автоматической обработки исходящих документов БЭСП(Макс.время)
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(statdate)                           as RDate,
					max(timeouttrans+timeintrans)             as Measure,
					P_SLA_ID                                  as SlaReportId
				FROM 
					vtb_bespstatistic
				WHERE 
						statdate >= v_intr_start 
					and statdate <  v_intr_end
					and timeouttrans > 0
					and timeintrans  > 0
				GROUP BY trunc(statdate);
				--ORDER BY 1
			END;
			
			
			WHEN 070 THEN BEGIN -- Время автоматической обработки исходящих документов БЭСП(Сред.время)
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(statdate)                           as "Дата",
					round(avg(timeouttrans+timeintrans),2)    as "Measure",
					P_SLA_ID                                  as "Sla Report ID"
				FROM 
					vtb_bespstatistic
				WHERE 
						statdate >= v_intr_start 
					and statdate <  v_intr_end
					and timeouttrans > 0
					and timeintrans  > 0
				GROUP BY trunc(statdate);
				--ORDER BY 1
			END;
			
			
			WHEN 080 THEN BEGIN -- Время обработки входящих документов БЭСП(Макс.время)
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(statdate)              as RDate,  
					round(max(timeauto/60),2)    as Measure,
					P_SLA_ID                     as SlaReportId
				FROM 
					vtb_bespstatistic
				WHERE 
						statdate >= v_intr_start 
					and statdate <  v_intr_end
					and timeauto > 0
				GROUP BY trunc(statdate);
				--ORDER BY 1
			END;
			
			
			WHEN 090 THEN BEGIN -- Время обработки входящих документов БЭСП(Сред.время)
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(statdate)              as RDate, 
					round(avg(timeauto/60),2)    as Measure, 
					P_SLA_ID                     as SlaReportId
				FROM 
					vtb_bespstatistic
				WHERE 
						statdate >= v_intr_start 
					and statdate <  v_intr_end
					and timeauto > 0
				GROUP BY trunc(statdate);
				--ORDER BY 1
			END;
			
			
			WHEN 100 THEN BEGIN -- Время обработки документов, поступивших по каналу SWIFT/ВТО
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(mi.dDate)		as RDate, 		
					avg(mi.cnt)         as Measure,
					P_SLA_ID            as SlaReportId
				FROM(
					SELECT 
						trunc(dv.visadate,'mi') dDate, 
						count(*) cnt
					FROM 
						doctree dt, 
						docvisa dv
					WHERE 
						dt.classified = dv.doc
						and dt.operdate >= v_intr_start
						and dt.operdate <  v_intr_end
						and dt.initdate >= v_intr_start
						and dt.category = 4
						and dv.entitymessage in (1000466754,1005424524)
						--and EXISTS (select 1 from docvisa dv1 where dv1.doc=dt.classified and dv1.entitystate=1000160979)
					GROUP BY trunc(dv.visadate,'mi') 
					HAVING count (*) > 500
					--ORDER BY 1
				) mi
				GROUP BY trunc(mi.dDate);
				--ORDER BY 1
			END;
			
			
			WHEN 110 THEN BEGIN -- Время обработки ЛОРО-ЛОРО документов(Среднее)
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(operdate)                  as RDate,
					(avg(INN-INITDATE)*24*60 + 
						avg(OUTT-INN)*24*60 + 
						avg(EXECC-OUTT)*24*60)/3    as Measure,
					P_SLA_ID                        as SlaReportId
				FROM(SELECT   /*+ index(doctree IX_DOCTREE_23) */
						dt.classified, 
						dt.initdate, 
						dt.operdate, 
						(SELECT min(dv.visadate)  FROM docvisa dv  WHERE dt.classified=dv.doc) AS INN,
						(SELECT min(dvv.visadate) FROM docvisa dvv WHERE dt.classified=dvv.doc and dvv.entitystate=1013245509 ) AS OUTT,
						(SELECT min(dvv.visadate) FROM docvisa dvv WHERE dt.classified=dvv.doc and dvv.entitystate=1013245138 ) AS EXECC
					FROM 
						doctree dt, 
						bankoper bo, 
						account ac, 
						account acc, 
						customertransfer ct
					WHERE 
						(dt.category=4 or  dt.category=15)
						and  dt.doctype in (1013245126)
						and dt.operdate >= v_intr_start
						and dt.operdate <  v_intr_end
						and dt.docstate=1000000035
						and dt.sysfilial=1
						and dt.classified=ct.doc
						and ct.nostro=acc.classified
						and dt.classified=bo.doc
						and bo.account=ac.classified
						and (substr(ac.code,1,5)='30109' or substr(ac.code,1,5)='30111')
						and (substr(acc.code,1,5)='30109' or substr(acc.code,1,5)='30111') 
						and bo.curraccount=1000001275
						and (
							SELECT 
								count(*) 
							FROM 
								docvisa ddvv 
							WHERE 
								ddvv.doc=dt.classified 
								and ddvv.userid not in ('DAEMON','NAQ')
							)=0
				) fg
				GROUP BY trunc(operdate);
				--ORDER BY 1
			END;
			
			
			WHEN 120 THEN BEGIN -- Время обработки ЛОРО-ЛОРО документов(Максимальное)
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(operdate)                                              as RDate,
					greatest( 
						greatest(avg(INN-INITDATE)*24*60, avg(OUTT-INN)*24*60), 
						avg(EXECC-OUTT)*24*60
					)                                                            as Measure,
					P_SLA_ID                                                     as SlaReportId
				FROM(SELECT   /*+ index(doctree IX_DOCTREE_23) */
						dt.classified, 
						dt.initdate, 
						dt.operdate, 
						(SELECT min(dv.visadate)  FROM docvisa dv  WHERE dt.classified=dv.doc) AS INN,
						(SELECT min(dvv.visadate) FROM docvisa dvv WHERE dt.classified=dvv.doc and dvv.entitystate=1013245509 ) AS OUTT,
						(SELECT min(dvv.visadate) FROM docvisa dvv WHERE dt.classified=dvv.doc and dvv.entitystate=1013245138 ) AS EXECC
					FROM 
						doctree dt, 
						bankoper bo, 
						account ac, 
						account acc, 
						customertransfer ct
					WHERE 
						(dt.category=4 or  dt.category=15)
						and  dt.doctype in (1013245126)
						and dt.operdate >= v_intr_start
						and dt.operdate <  v_intr_end
						and dt.docstate=1000000035
						and dt.sysfilial=1
						and dt.classified=ct.doc
						and ct.nostro=acc.classified
						and dt.classified=bo.doc
						and bo.account=ac.classified
						and (substr(ac.code,1,5)='30109' or substr(ac.code,1,5)='30111')
						and (substr(acc.code,1,5)='30109' or substr(acc.code,1,5)='30111') 
						and bo.curraccount=1000001275
						and (
							SELECT 
								count(*) 
							FROM 
								docvisa ddvv 
							WHERE 
								ddvv.doc=dt.classified 
								and ddvv.userid not in ('DAEMON','NAQ')
							)=0
				) fg
				GROUP BY trunc(operdate);
				--ORDER BY 1
			END;
			
			
			WHEN 130 THEN BEGIN -- Отчет: Справка о расчетах по счетам 47416/47417
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(eventtime)    as RDate,
					--label,
					round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
						  to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
						  to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
						  * 60          as Measure,        
					P_SLA_ID	        as SlaReportId
				FROM 
					eventrecord
				WHERE 
						eventtime >= v_intr_start
					and eventtime <  v_intr_end
					and eventtype = 60000 -- запуск отчета
					and description like '%отчета: __:__:__%'
					and (label like '%BBR_VTBRC_SRP_47416_47417)%')
				GROUP BY trunc(eventtime), label
				HAVING round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
							 to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
							 to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
							 > 0;
				--ORDER BY DT
			END;
			
			
			WHEN 140 THEN BEGIN -- Отчет: Ведомость расшифровка по счету 47416
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(eventtime) dt, 
					--label,
					round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
							  to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
							  to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
							  *60
							  avgdur,
					P_SLA_ID
				FROM 
					eventrecord
				WHERE 
						eventtime >= v_intr_start
					and eventtime <  v_intr_end
					and eventtype = 60000 -- запуск отчета
					and description like '%отчета: __:__:__%'
					and (label like '%BBR_ABOUT_REESTR_ACCOUNT)%')
				GROUP BY trunc(eventtime), label
				HAVING round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
								 to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
								 to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
								 > 0;
				--order by dt
			END;
			
			
			WHEN 150 THEN BEGIN -- Отчет: Все раккорды
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(eventtime) dt, 
					--label,
					round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
							  to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
							  to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
							  avgdur,
					P_SLA_ID
					FROM 
						eventrecord
					WHERE 
							eventtime >= v_intr_start
						and eventtime <  v_intr_end
						and eventtype = 60000 -- запуск отчета
						and description like '%отчета: __:__:__%'
						and label like '%BBR_VTB_TIKH02_ALLRAKK)%'
					GROUP BY trunc(eventtime), label
					HAVING round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
									 to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
									 to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
									 > 0;
					--order by 2,1
			END;
			
			
			WHEN 160 THEN BEGIN -- Отчет: 72. Раккорд
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(eventtime) dt, 
					--label,
					round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
							  to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
							  to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
							  avgdur,
					P_SLA_ID
				FROM 
					eventrecord
				WHERE 
						eventtime >= v_intr_start
					and eventtime <  v_intr_end
					and eventtype = 60000 -- запуск отчета
					and description like '%отчета: __:__:__%'
					and label like '%BBR_VTBRC_RAKKORD_TIKHONOV)%'
				GROUP BY trunc(eventtime), label
				HAVING round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
								 to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
								 to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
								 > 0;
				--order by 2,1
			END;
			
			
			WHEN 170 THEN BEGIN -- Отчет: Филиалы не приславшие остатки
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(eventtime) dt, 
					--label,
					round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
							  to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
							  to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2)
							  *60						  
							  avgdur,
					P_SLA_ID
				FROM 
					eventrecord
				WHERE 
						eventtime >= v_intr_start
					and eventtime <  v_intr_end
					and eventtype = 60000 -- запуск отчета
					and description like '%отчета: __:__:__%'
					and (label like '%BBR_FILIALNOBALANCE)%')
				GROUP BY trunc(eventtime), label
				HAVING round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
								 to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
								 to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
								 > 0;
				--ORDER BY DT
			END;
			
			
			WHEN 180 THEN BEGIN -- Отчет: Количество документов в очереди НОСТРО
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(eventtime) dt, 
					--label,
					round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
							  to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
							  to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
							  avgdur,
					P_SLA_ID
				FROM 
					eventrecord
				WHERE 
						eventtime >= v_intr_start
					and eventtime <  v_intr_end
					and eventtype = 60000 -- запуск отчета
					and description like '%отчета: __:__:__%'
					and label like '%BBR_VTB_QUEUE_VF_NOSTRO)%'
				GROUP BY trunc(eventtime), label
				HAVING round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
								 to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
								 to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
								 > 0;
				--order by 2,1
			END;
			
			
			WHEN 190 THEN BEGIN -- Отчет: Сверка оборотов НА и РКЦ
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(eventtime) dt, 
					--label,
					round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
							  to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
							  to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2)
							  *60
							  avgdur,
					P_SLA_ID
				FROM 
					eventrecord
				WHERE 
						eventtime >= v_intr_start
					and eventtime <  v_intr_end
					and eventtype = 60000 -- запуск отчета
					and description like '%отчета: __:__:__%'
					and (label like '%BBR_VTBR_SVERKA_NA)%')
				GROUP BY trunc(eventtime), label
				HAVING round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
								 to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
								 to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
								 > 0;
				--order by dt;
			END;
			
			
			WHEN 200 THEN BEGIN -- Отчет: Филиалы, не приславшие остатки по рублям
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(eventtime) dt, 
					--label,
					round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
							  to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
							  to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
							  *60
							  avgdur,
					P_SLA_ID
				FROM 
					eventrecord
				WHERE 
						eventtime >= v_intr_start
					and eventtime <  v_intr_end
					and eventtype = 60000 -- запуск отчета
					and description like '%отчета: __:__:__%'
					and (label like '%BBR_FILIALNOBALANCE_RUR)%')
				GROUP BY trunc(eventtime), label
				HAVING round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
								 to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
								 to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
								 > 0;
			END;
			
			
			WHEN 210 THEN BEGIN -- Отчет: Окончательная выписка в ОПЕРУ Москва
			INSERT INTO VTB_STAT_SLA_DATA(date_hh24, measure, sla_id)
				SELECT 
					trunc(eventtime) dt, 
					--label,
					round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
							  to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
							  to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
							  *60
							  avgdur,
					P_SLA_ID
				FROM 
					eventrecord
				WHERE 
						eventtime >= v_intr_start
					and eventtime <  v_intr_end
					and eventtype = 60000 -- запуск отчета
					and description like '%отчета: __:__:__%'
					and (label like '%BBR_VTBRC_RMAKET_EXTRACT_2P)%')
				GROUP BY trunc(eventtime), label
				HAVING round(avg(to_number(substr(description, instr(description, 'отчета: ') + 8, 2))*3600 +
								 to_number(substr(description, instr(description, 'отчета: ') + 11, 2))*60 +
								 to_number(substr(description, instr(description, 'отчета: ') + 14, 2)))/60, 2) 
								 > 0;
				--order by dt
			END;
			
		END CASE;
		-- конец сбора статистики
	
	
	WriteLog(P_SLA_ID, 'Сборка прошла успешно!');

	
	END CollectData;

----------------------------------------------------------------

BEGIN

	-- точка восстановления - исходное состояние БД до запуска процедуры - в случае 
	-- какой бы то ни было исключительной ситуации(Exception`а)
	SAVEPOINT VIRGIN;


	-- чтение идентификаторов SLA-метрик из информационной таблицы
	OPEN SlaInfoCur;
	LOOP
		FETCH SlaInfoCur INTO SlaInfoCur_id;
		EXIT WHEN SlaInfoCur%NOTFOUND;
		CollectData(SlaInfoCur_id);		
	END LOOP;


	-- либо Функция отрабатывает успешно на ВСЕМ интервале для ВСЕХ SLA,
	-- либо ни на каком интервале дат, ни для какого SLA
	COMMIT;


	-- удаление выходных дней
	DELETE FROM VTB_STAT_SLA_DATA WHERE date_hh24 IN(
		SELECT holidaydate FROM HOLIDAY WHERE holidayschema = 1000131227 -- регион: Россия
		AND holidaydate BETWEEN G_INTERVAL_START AND G_INTERVAL_END
		);
	COMMIT;


EXCEPTION

	WHEN OTHERS THEN BEGIN		
		WriteLog(-1, 'Произошла ошибка. Код ошибки: ' || SQLCODE || ' | ' || SUBSTR(SQLERRM, 1, 200));
		ROLLBACK TO VIRGIN;
	END;
	
END DPC_VTB_SlaCollectData;
/