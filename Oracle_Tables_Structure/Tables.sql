﻿-- Очистка окружения
BEGIN
 EXECUTE IMMEDIATE 'DROP TABLE VTB_STAT_SLA_DATA';
EXCEPTION
 WHEN OTHERS THEN IF SQLCODE != -942 THEN RAISE; END IF;
END;
/
BEGIN
 EXECUTE IMMEDIATE 'DROP TABLE VTB_STAT_SLA_INFO';
EXCEPTION
 WHEN OTHERS THEN IF SQLCODE != -942 THEN RAISE; END IF;
END;
/



--------------------------------------------------------------------



-- Таблица хранения данных о различных Service Layer Agreement метриках и их составляющих(2 и более графиков)
CREATE TABLE VTB_STAT_SLA_INFO(
       -- Уникальный идентификатор SLA-метрики:
       sla_id           integer not null,
       -- Название SLA-метрики:
       sla_report_name  varchar2(256) not null,
       -- Принятый за соглашение показатель SLA:
       sla_std_measure  float not null check(sla_std_measure > 0),
       -- Единица измерения метрики(human-readable):
       sla_measure_unit varchar(256) not null,
       
       CONSTRAINT pk_sla_info      PRIMARY KEY(sla_id),
       -- Название отчетности должно быть уникальным
       CONSTRAINT uniq_report_name UNIQUE(sla_report_name) 
       );  
   
-- Добавление комментрариев к столбцам таблицы 
COMMENT ON COLUMN VTB_STAT_SLA_INFO.sla_id           IS 'Уникальный идентификатор SLA-метрики';
COMMENT ON COLUMN VTB_STAT_SLA_INFO.sla_report_name  IS 'Название SLA-метрики(тайтл графика) в формате: НазваниеГруппы[НазваниеСерии]';
COMMENT ON COLUMN VTB_STAT_SLA_INFO.sla_std_measure  IS 'Принятый за соглашение показатель SLA';
COMMENT ON COLUMN VTB_STAT_SLA_INFO.sla_measure_unit IS 'Единица измерения метрики(название Y-оси на конечном графике)';	   
	  
-- Заполнение информационной таблицы статикой 
-- Формат имени отчета: 'НазваниеГруппы[НазваниеМетрики]'
-- "НазваниеГруппы" - названия отчетов, которые нужно отображать на одном графике(группировка)
-- "НазваниеСерии" - название линии в группе отчетов(то, что будет в легенде графика)
-- (если строки получаются слишком длинными, в них следует добавлять пробелы, 
-- чтобы их можно было "красиво" разбить и отобразить на графике)
CREATE OR REPLACE SYNONYM SLAINFO FOR VTB_STAT_SLA_INFO;
INSERT INTO SLAINFO VALUES(010, 'Скорость загрузки входящих документов МЦИ[Количество (док/мин)]',   50,  'Количество(док/мин)');
INSERT INTO SLAINFO VALUES(020, 'Скорость загрузки входящих документов SWIFT[Количество (док/сек)]', 3,   'Количество(док/сек)');
INSERT INTO SLAINFO VALUES(030, 'Скорость загрузки входящих документов TELEX[Количество (док/мин)]', 0.5, 'Количество(док/сек)');
INSERT INTO SLAINFO VALUES(040, 'Время создания/подписания расчетных документов[Создание 1 рука]',   5, 'Время(сек)');
INSERT INTO SLAINFO VALUES(050, 'Время создания/подписания расчетных документов[Подписание 2 рука]', 5, 'Время(сек)');
INSERT INTO SLAINFO VALUES(060, 'Время автоматической обработки исходящих документов БЭСП[Максимальное время обработки]', 120, 'Время(сек)'); 
INSERT INTO SLAINFO VALUES(070, 'Время автоматической обработки исходящих документов БЭСП[Среднее время обработки]', 120, 'Время(сек)');
INSERT INTO SLAINFO VALUES(080, 'Время обработки входящих документов БЭСП[Максимальное время обработки]', 15, 'Время(мин)');
INSERT INTO SLAINFO VALUES(090, 'Время обработки входящих документов БЭСП[Среднее время обработки]', 15, 'Время(мин)');
INSERT INTO SLAINFO VALUES(100, 'Время обработки документов, поступивших по каналу SWIFT/ВТО[Количество(док/мин)]', 450, 'Количество (док/мин)');
INSERT INTO SLAINFO VALUES(110, 'Время обработки ЛОРО-ЛОРО документов[Среднее]',      120, 'Время(мин)'); 
INSERT INTO SLAINFO VALUES(120, 'Время обработки ЛОРО-ЛОРО документов[Максимальное]', 120, 'Время(мин)'); 
INSERT INTO SLAINFO VALUES(130, 'Отчет: Справка о расчетах по счетам 47416/47417[Справка о расчетах по счетам 47416/47417]', 180, 'Время(сек)');
INSERT INTO SLAINFO VALUES(140, 'Отчет: Ведомость расшифровка по счету 47416[Ведомость расшифровка по счету 47416]', 240, 'Время(сек)');
INSERT INTO SLAINFO VALUES(150, 'Отчет: Все раккорды[Все раккорды]',  120, 'Время выполнения(мин)');
INSERT INTO SLAINFO VALUES(160, 'Отчет: 72. Раккорд[72. Раккорд]',   120, 'Время(сек)');
INSERT INTO SLAINFO VALUES(170, 'Отчет: Филиалы не приславшие остатки[Филиалы не приславшие остатки]', 60, 'Время(сек)');
INSERT INTO SLAINFO VALUES(180, 'Отчет: Количество документов в очереди НОСТРО[Количество документов в очереди НОСТРО]', 60, 'Время(сек)');
INSERT INTO SLAINFO VALUES(190, 'Отчет: Сверка оборотов НА и РКЦ[Сверка оборотов НА и РКЦ]', 60, 'Время(сек)');
INSERT INTO SLAINFO VALUES(200, 'Отчет: Филиалы, не приславшие остатки по рублям[Филиалы, не приславшие остатки по рублям]', 60, 'Время(сек)');
INSERT INTO SLAINFO VALUES(210, 'Отчет: Окончательная выписка в ОПЕРУ Москва[Окончательная выписка в ОПЕРУ Москва]', 60, 'Время(сек)');
DROP SYNONYM SLAINFO;

COMMIT;


      
--------------------------------------------------------------------



-- Таблица сбора промежуточной статистики показателей SLA  
CREATE TABLE VTB_STAT_SLA_DATA(
       -- Дата сбора локальной статистики SLA за конкретное число(мин.интервал=24часа)
       date_hh24  date not null,
       -- Идентификатор SLA-метрики, по которому была собрана статистика
       sla_id     integer not null,
       -- Результат измерения
       measure    float not null check(measure >= 0),
       
       CONSTRAINT fk_sla_id    FOREIGN KEY(sla_id) REFERENCES VTB_STAT_SLA_INFO,
       -- За каждую дату должно быть ЕДИНСТВЕННОЕ измерение SLA-метрики
       -- CONSTRAINT uniq_data UNIQUE(sla_id, date_hh24)
	   CONSTRAINT pk_sla_data  PRIMARY KEY(sla_id, date_hh24)
       );
	   
-- Добавление комментрариев к столбцам таблицы 
COMMENT ON COLUMN VTB_STAT_SLA_DATA.date_hh24 IS 'Дата сбора локальной статистики SLA за конкретное число(мин.интервал=24часа)';
COMMENT ON COLUMN VTB_STAT_SLA_DATA.sla_id    IS 'Идентификатор SLA-метрики, по которому была собрана статистика';
COMMENT ON COLUMN VTB_STAT_SLA_DATA.measure   IS 'Результат измерения SLA по дате';



--------------------------------------------------------------------



-- Общедоступные alias`ы для таблиц
CREATE OR REPLACE PUBLIC SYNONYM VTB_STAT_SLA_INFO FOR VTB_STAT_SLA_INFO;
CREATE OR REPLACE PUBLIC SYNONYM VTB_STAT_SLA_DATA FOR VTB_STAT_SLA_DATA;