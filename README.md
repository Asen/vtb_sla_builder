### What is it? ###

This repo contains all the sources related to VTB Bank "SLA Automation" project.
This project helps Bank's IT department to build weekly/monthly/quarterly/etc SLA reports very fast and without any extra load on bank's Oracle Database.

### What is inside? ###

1. **C#_Application** folder contains the С#-sources of application which builds XLS-reports from XML-files in GUI mode.

2. **Oracle_Tables_Structure** folder contains the SQL-script which deploys 2 additional tables in Bank`s Oracle DB

3. **Oracle_Trigger_Procedure** folder contains the SQL-Trigger(procedure) for Oracle Job. This procedure determines all the logic of collecting SLA data. Job(trigger) runs 1 time per day(24h).