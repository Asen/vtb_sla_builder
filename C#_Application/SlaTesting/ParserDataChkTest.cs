﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SlaReportsBuilder;
using System.IO;

namespace SlaTesting
{
    [TestClass]
    public class ParserDataChkTest
    {
        String dirName = @"..\..\TestData\ParserDataChkTest";

        [TestMethod]
        public void CheckNoRootDetected()
        {
            bool isDetected = false;
            try
            {
                String file = Path.Combine(dirName, "wrong_xml_root.xml");
                SlaDocument parser = new SlaDocument(file);
                parser.Parse();
            }
            catch (Exception e)
            {
                if(e.Message.IndexOf(ErrorCode.XmlNoRootElementFound.GetMessage()) != -1)
                    isDetected = true;
            }

            Assert.IsTrue(isDetected);
        }


        [TestMethod]
        public void CheckNoIdNodeDetected()
        {
            bool isDetected = false;
            try
            {
                String file = Path.Combine(dirName, "wrong_report_no_id_node.xml");
                SlaDocument parser = new SlaDocument(file);
                parser.Parse();
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf(ErrorCode.XmlNoReportInfoElementFound.GetMessage()) != -1)
                    isDetected = true;
            }

            Assert.IsTrue(isDetected);
        }


        [TestMethod]
        public void CheckNoNameNodeDetected()
        {
            bool isDetected = false;
            try
            {
                String file = Path.Combine(dirName, "wrong_report_no_name_node.xml");
                SlaDocument parser = new SlaDocument(file);
                parser.Parse();
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf(ErrorCode.XmlNoReportInfoElementFound.GetMessage()) != -1)
                    isDetected = true;
            }

            Assert.IsTrue(isDetected);
        }


        [TestMethod]
        public void CheckNoMeasureNodeShouldNotBeDetected()
        {
            bool isDetected = true;
            try
            {
                String file = Path.Combine(dirName, "wrong_report_no_measure_node.xml");
                SlaDocument parser = new SlaDocument(file);
                parser.Parse();
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf(ErrorCode.XmlNoReportInfoElementFound.GetMessage()) != -1)
                    isDetected = false;
            }

            Assert.IsTrue(isDetected);
        }


        [TestMethod]
        public void CheckNoUnitNodeDetected()
        {
            bool isDetected = false;
            try
            {
                String file = Path.Combine(dirName, "wrong_report_no_unit_node.xml");
                SlaDocument parser = new SlaDocument(file);
                parser.Parse();
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf(ErrorCode.XmlNoReportInfoElementFound.GetMessage()) != -1)
                    isDetected = true;
            }

            Assert.IsTrue(isDetected);
        }



        [TestMethod]
        public void CheckNoDataNodeDetected()
        {
            bool isDetected = false;
            try
            {
                String file = Path.Combine(dirName, "wrong_report_no_data_node.xml");
                SlaDocument parser = new SlaDocument(file);
                parser.Parse();
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf(ErrorCode.XmlNoDataElementFound.GetMessage()) != -1)
                    isDetected = true;
            }

            Assert.IsTrue(isDetected);
        }


        [TestMethod]
        public void CheckIncorrectDigitMeasureNode()
        {
            bool isDetected = false;
            try
            {
                String file = Path.Combine(dirName, "wrong_report_incorrect_digit_1.xml");
                SlaDocument parser = new SlaDocument(file);
                parser.Parse();
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf(ErrorCode.XmlDigitConversion.GetMessage()) != -1)
                    isDetected = true;
            }

            Assert.IsTrue(isDetected);
        }


        [TestMethod]
        public void CheckIncorrectDigitDataNode()
        {
            bool isDetected = false;
            try
            {
                String file = Path.Combine(dirName, "wrong_report_incorrect_digit_2.xml");
                SlaDocument parser = new SlaDocument(file);
                parser.Parse();
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf(ErrorCode.XmlDigitConversion.GetMessage()) != -1)
                    isDetected = true;
            }

            Assert.IsTrue(isDetected);
        }


        [TestMethod]
        public void CheckSlaIdMustBeInt()
        {
            bool isDetected = false;
            try
            {
                String file = Path.Combine(dirName, "wrong_report_incorrect_digit_3.xml");
                SlaDocument parser = new SlaDocument(file);
                parser.Parse();
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf(ErrorCode.XmlDigitConversion.GetMessage()) != -1)
                    isDetected = true;
            }

            Assert.IsTrue(isDetected);
        }


        [TestMethod]
        public void CheckIncorrectDigitFormat()
        {
            String file = Path.Combine(dirName, "wrong_report_incorrect_digit_formats.xml");
            SlaDocument parser = new SlaDocument(file);
            parser.Parse();
        }


        [TestMethod]
        public void CheckIncorrectDateFormat()
        {
            bool isDetected = false;
            try
            {
                String file = Path.Combine(dirName, "wrong_report_incorrect_date_format.xml");
                SlaDocument parser = new SlaDocument(file);
                parser.Parse();
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf(ErrorCode.XmlDateConversion.GetMessage()) != -1)
                    isDetected = true;
            }

            Assert.IsTrue(isDetected);
        }


        [TestMethod]
        public void CheckNoDateAttrDetected()
        {
            bool isDetected = false;
            try
            {
                String file = Path.Combine(dirName, "wrong_report_no_date_attr.xml");
                SlaDocument parser = new SlaDocument(file);
                parser.Parse();
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf(ErrorCode.XmlNoDateAttributeFound.GetMessage()) != -1)
                    isDetected = true;
            }

            Assert.IsTrue(isDetected);
        }
    }
}
