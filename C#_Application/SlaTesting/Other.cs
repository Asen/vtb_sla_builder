﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SlaReportsBuilder;
using System.Collections.Generic;
using System.Linq;

namespace SlaTesting
{
    [TestClass]
    public class Other
    {
        [TestMethod]
        public void CheckDataClassIndexing()
        {
            SlaData a = new SlaData(new DateTime(2016, 8, 30, 12, 11, 56));
            SlaData b = new SlaData(new DateTime(2016, 8, 30, 17, 29, 04), int.MaxValue);
   
            Assert.AreEqual(a, b, "Объекты не должны зависеть от времени!");

            SlaData c = new SlaData(new DateTime(2016, 8, 29));
            SlaData d = new SlaData(new DateTime(2016, 8, 31));

            HashSet<SlaData> sortCheck = new HashSet<SlaData>();
            sortCheck.Add(c);
            sortCheck.Add(a);
            sortCheck.Add(d);
            sortCheck.Add(b);

            Assert.IsTrue(sortCheck.Count == 3, "Индексация класса происходит некорректно!");
            Assert.IsTrue(sortCheck.ElementAt(0) == c, "Индексация класса происходит некорректно!");
            Assert.IsTrue(sortCheck.ElementAt(1) == a, "Индексация класса происходит некорректно!");
            Assert.IsTrue(sortCheck.ElementAt(2) == d, "Индексация класса происходит некорректно!");
        }

        [TestMethod]
        public void CheckReportCodeExtractione()
        {
            SlaReport S1 = new SlaReport(10, "    НаЗвАнИе      отчета  \"№1\"    [Spec1]", 10, "");
            SlaReport S2 = new SlaReport(20, "Название отчета \"№1\"", 10, "");
            Assert.AreEqual(S1.GenGroupNameCode(), S2.GenGroupNameCode(), "Коды групп не эквивалентны!");
        }

        [TestMethod]
        public void CheckReportFullnameExtraction()
        {
            SlaReport S1 = new SlaReport(10, "    НаЗвАнИе      отчета  \"№1\"    [Spec1]", 10, "");
            Assert.AreEqual(S1.ExtractGroupName(), "НаЗвАнИе      отчета  \"№1\"", "Название группы не то!");
        }

        [TestMethod]
        public void CheckReportSpecExtraction()
        {
            SlaReport S1 = new SlaReport(10, "    НаЗвАнИе      отчета  \"№1\"    [       SpEc1 324 (ffffff) ]", 10, "");
            Assert.AreEqual(S1.ExtrctReportSpec(), "SpEc1 324 (ffffff)", "CСпецификация группы не такая!");
        }

        [TestMethod]
        public void CheckStringToDigitCulturalConversion()
        {
            string digitA = " 500 298 , 7  ";
            float digitA_ = 0;
            Helper.CulturalStrToFloat(digitA, out digitA_);
            Assert.AreEqual(digitA_, 500298.7f, "Неверная конвертация в число 1!");

            digitA = "1 000.9";
            digitA_ = 0;
            Helper.CulturalStrToFloat(digitA, out digitA_);
            Assert.AreEqual(digitA_, 1000.9f, "Неверная конвертация в число 2!");

            digitA = "1000.9";
            digitA_ = 0;
            Helper.CulturalStrToFloat(digitA, out digitA_);
            Assert.AreEqual(digitA_, 1000.9f, "Неверная конвертация в число 3!");
        }
    }
}
