﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SlaReportsBuilder;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SlaTesting
{
    [TestClass]
    public class ParserLogic
    {
        const string dirName = @"..\..\TestData\ParserLogicTest\";

        [TestMethod]
        public void CheckNotEmptyGroups()
        {
            foreach (String XML in Directory.GetFiles(dirName, "*.xml"))
            {
                SlaDocument parser = new SlaDocument(XML);
                parser.Parse();
                foreach (List<SlaReport> group in parser)
                {
                    Assert.IsTrue(group.Count > 0, "В группе 0 отчетов!");
                }
            }           
        }

        [TestMethod]
        public void CheckGroupDataCountEquality()
        {
            foreach (String XML in Directory.GetFiles(dirName, "*.xml"))
            {
                SlaDocument parser = new SlaDocument(XML);
                parser.Parse();

                foreach (List<SlaReport> group in parser)
                {
                    for (int i = 0; i < group.Count - 1; i++)
                    {
                        bool isEquals = group[i].GetDataSize() == group[i + 1].GetDataSize();
                        Assert.IsTrue(isEquals, "Неравное количество значений в отчетах одной группы!");
                    }
                }
            }
        }


        [TestMethod]
        public void CheckGroupDataIdentical()
        {
            foreach (String XML in Directory.GetFiles(dirName, "*.xml"))
            {
                SlaDocument parser = new SlaDocument(XML);
                parser.Parse();

                foreach (List<SlaReport> group in parser)
                {
                    for (int i = 0; i < group.Count - 1; i++)
                    {
                        bool isSimilarCollections = group[i].GetAllData().SequenceEqual(group[i + 1].GetAllData());
                        Assert.IsTrue(isSimilarCollections, "Наборы данных не идентичны!");
                    }
                }
            }
        }


        [TestMethod]
        public void CheckGrouping()
        {
            String XML = Path.Combine(dirName, "grouping_test.xml");

            SlaDocument parser = new SlaDocument(XML);
            parser.Parse();

            List<List<SlaReport>> groups = new List<List<SlaReport>>(parser);
            bool isGroupSingle = groups.Count == 1;
            Assert.IsTrue(isGroupSingle, "Должна получиться единственная группа!");

            List<SlaReport> group = groups[0];
            bool isGroupHave3Reports = group.Count == 3;
            Assert.IsTrue(isGroupHave3Reports, "В группе должно быть 3 отчета!");

            HashSet<SlaData> integrity = new HashSet<SlaData>(new[]{
                new SlaData(new DateTime(2016, 4, 1)),
                new SlaData(new DateTime(2016, 4, 2)),
                new SlaData(new DateTime(2016, 4, 3)),
                new SlaData(new DateTime(2016, 4, 4)),
                new SlaData(new DateTime(2016, 4, 5)),
                new SlaData(new DateTime(2016, 4, 6)),
                new SlaData(new DateTime(2016, 4, 7)),
                new SlaData(new DateTime(2016, 4, 8))
            });

            foreach (SlaReport rep in group)
            {
                Assert.IsTrue(integrity.Count() == 8, "Некорректный набор - должно быть 8 дат");
                Assert.IsTrue(rep.GetDataSize() == integrity.Count, "Не то количество данных!");
                Assert.IsTrue(rep.GetAllData().SequenceEqual(integrity), "Массив данных неверный!");
            }
        }

    }
}
