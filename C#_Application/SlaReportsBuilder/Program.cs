﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace SlaReportsBuilder
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            // Рабочая директория
            string cwd = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData); 
            Directory.SetCurrentDirectory(cwd);

            // Run, Forest, run!
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new SlaBuilderMainForm());
        }

        public static string GetAppPath()
        {
            string[] argv = Environment.GetCommandLineArgs();
            string path = argv.Length > 0 ? argv[0] : Process.GetCurrentProcess().MainModule.FileName;
            return Path.GetDirectoryName(path);
        }
    }
}
