﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace SlaReportsBuilder
{
    // Класс, реализующий логику по управлению содержимым файла выгрузки
    public class SlaDocument : IEnumerable<List<SlaReport>>
    {
        // Типы сообщений, записываемых в лог
        private enum WarningType { EmptyData, TooMuchEmptiesAdded };

        // Имя файла, который будет парситься
        private string unloadFile = null;
        // Глобальный конфиг XLS
        private SlaDocumentConfig config = new SlaDocumentConfig();
        // Сгруппированный буфер всех отчетов из файла выгрузки       
        private Dictionary<int, List<SlaReport>> reportGroups = new Dictionary<int, List<SlaReport>>();
        // Лог парсера
        private List<string> warningsLog = new List<string>();


        public SlaDocument(string fileName)
        {
            unloadFile = fileName;
        }


        public void Parse()
        {
            _PrepareParser();
            _ParseFile();                 
        }


        public bool IsLogEmpty()
        {
            return warningsLog.Count == 0;
        }

        
        public string GetWarningsLog()
        {
            StringBuilder log = new StringBuilder();
            int maxLogToDisplay = 5;
            int displayedLogs = Math.Min(warningsLog.Count, maxLogToDisplay);

            for (int i = 0; i < displayedLogs; i++)
                log.Append(warningsLog[i] + (i==displayedLogs-1 ? "" : "\n\n"));

            if (warningsLog.Count > maxLogToDisplay)
                log.Append("\n\n(и еще " + (warningsLog.Count - maxLogToDisplay) + " сообщений...)");

            return log.ToString();
        }


        public SlaDocumentConfig GetConfig()
        {
            return config;
        }


        // Добавление warning-лога в лог-буфер
        private void _WriteLog(WarningType logType, SlaReport report)
        {
            int logId = warningsLog.Count + 1;

            switch (logType)
            {
                case WarningType.EmptyData:
                    warningsLog.Add(
                        logId + ") У отчета \"" + report.GetFullName() + 
                        "\" отсутствуют данные для построения графика.");
                    break;

                case WarningType.TooMuchEmptiesAdded:
                    warningsLog.Add(
                        logId + ") В отчет \"" + report.GetFullName() +
                        "\" было добавлено более 50% пустых данных "+
                        "всвязи с сильным расхождением данных по дате в других отчетах, входящих в группу "+
                        "(даты отчетов, рисуемых на одном графике должны быть в целом схожи).");
                    break;
            }
        }


        // Подготовка к процессу парсинга
        private void _PrepareParser()
        {
            reportGroups.Clear();
            warningsLog.Clear();
        }


        // Парсинг XML-файла 
        // (XML нотация умышленно инкапсулирована внутри класса)
        private void _ParseFile()
        {                       
            XmlDocument xdoc = new XmlDocument();
            Encoding CP1251 = Encoding.GetEncoding(1251);        // На вход подается файл в кодировке Windows-1251
            try
            {
                xdoc.LoadXml(File.ReadAllText(unloadFile, CP1251));
            }
            catch (XmlException xmlMalFormedException)
            {
                throw new SlaHandledException(ErrorCode.XmlNoRootElementFound, xmlMalFormedException);
            }

            // Кореневой узел, инкапсулирующий все отчеты SLA
            XmlNode xReports = xdoc.DocumentElement.SelectSingleNode("/sla/reports");
            if (xReports == null)
                throw new SlaHandledException(ErrorCode.XmlNoRootElementFound);
            else
                _ReadReportsNode(xReports);

            // Необязательный узел, содержащий конфигурацию для всего XLS-документа
            XmlNode xConfig = xdoc.DocumentElement.SelectSingleNode("/sla/global_config");
            if (xConfig != null)
                _ReadConfigNode(xConfig);
        }


        // Чтение отчетов из XML-объекта
        private void _ReadReportsNode(XmlNode xReportsNode)
        {
            HashSet<int> integrityIdBuffer = new HashSet<int>(); // контроль уникальности ключей отчетов

            foreach (XmlNode xmlReport in xReportsNode.ChildNodes)
            {
                int reportId;
                float slaStdMeasure = float.NaN;
                string slaMeasureUnit = string.Empty;
                string reportName = string.Empty;
                string chartType = string.Empty;

                // Атрибут "type" узла -- необязательный
                XmlAttribute chart_type_attr = xmlReport.Attributes["type"];
                if (chart_type_attr != null) chartType = chart_type_attr.Value;

                // Узел <sla_report_name>
                XmlNode report_name_Node = xmlReport.SelectSingleNode("sla_report_name");
                if (report_name_Node == null)
                    throw new SlaHandledException(ErrorCode.XmlNoReportInfoElementFound, "узел <sla_report_name>");
                reportName = report_name_Node.InnerText;
                report_name_Node = null;

                // Узел <sla_id>
                XmlNode sla_id_Node = xmlReport.SelectSingleNode("sla_id");
                if (sla_id_Node == null)
                    throw new SlaHandledException(ErrorCode.XmlNoReportInfoElementFound, "узел <sla_id>" + "\nОтчет: \"" + reportName +"\"");
                if (!int.TryParse(sla_id_Node.InnerText, out reportId))
                    throw new SlaHandledException(ErrorCode.XmlDigitConversion);
                sla_id_Node = null;
                                
                // Узел <sla_std_measure> -- необязательный
                XmlNode std_measure_Node = xmlReport.SelectSingleNode("sla_std_measure");
                if(std_measure_Node != null
                    && !Helper.CulturalStrToFloat(std_measure_Node.InnerText, out slaStdMeasure))
                        throw new SlaHandledException(ErrorCode.XmlDigitConversion);
                std_measure_Node = null;
                           
                // Узел <sla_measure_unit>
                XmlNode measure_unit_Node = xmlReport.SelectSingleNode("sla_measure_unit");
                if (measure_unit_Node == null)
                    throw new SlaHandledException(ErrorCode.XmlNoReportInfoElementFound, "узел <sla_measure_unit>" + "\nОтчет: \"" + reportName + "\"");
                slaMeasureUnit = measure_unit_Node.InnerText;
                measure_unit_Node = null;

                //////////////////// Создание объекта отчета /////////////////////////  
                                       
                SlaReport slaReport = new SlaReport(reportId, reportName, slaStdMeasure, slaMeasureUnit);
                slaReport.SetChartType(chartId: chartType);

                // Контроль дубликтов ключей отчетов
                foreach (List<SlaReport> uniqs in reportGroups.Values)
                    if (uniqs.Contains(slaReport))
                        throw new SlaHandledException(ErrorCode.XmlReportKeyDuplicate, "узел <sla_id>" + "\nОтчет: \"" + reportName + "\"");

                /////////////////////////////////////////////////////////////////////  

                // Узел со всеми данными по отдельному отчету
                XmlNode dataSection = xmlReport.SelectSingleNode("data");
                if (dataSection == null)
                    throw new SlaHandledException(ErrorCode.XmlNoDataElementFound, "Отчет: \"" + reportName + "\"");

                // Обработка массива данных кахдого отчета
                foreach (XmlNode measureBlock in dataSection.ChildNodes)
                {
                    XmlAttribute dateAttr = measureBlock.Attributes["date"];
                    if (dateAttr == null)
                        throw new SlaHandledException(ErrorCode.XmlNoDateAttributeFound, "Отчет: \"" + reportName + "\"");

                    // Атрибут "date" узла <measure>
                    DateTime date;
                    if (!DateTime.TryParseExact(dateAttr.Value, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                        throw new SlaHandledException(ErrorCode.XmlDateConversion, "Отчет: \"" + reportName + "\"");

                    // Значение узла <measure>
                    float measure;
                    if (!Helper.CulturalStrToFloat(measureBlock.InnerText, out measure))
                        throw new SlaHandledException(ErrorCode.XmlDigitConversion, "блок <data> узел <measure>" + "\nОтчет: \"" + reportName + "\"");

                    slaReport.AppendData(new SlaData(date, measure));
                }

                // WARNING!
                if (slaReport.GetDataSize() == 0)
                    _WriteLog(WarningType.EmptyData, slaReport);

                // Добавление нового отчета в группу по имени группы
                int groupCode = slaReport.GenGroupNameCode().GetHashCode();
                if (!reportGroups.ContainsKey(groupCode))
                    reportGroups.Add(groupCode, new List<SlaReport>());
                reportGroups[groupCode].Add(slaReport);
            }

            // Группировка отчетов по дате
            _JoinReportsInGroupByDate();
        }


        // Чтение конфигурации XLS из XML-объекта
        private void _ReadConfigNode(XmlNode xConfigNode)
        {
            XmlNode docCaption = xConfigNode.SelectSingleNode("doc_caption");
            if (docCaption != null)
                config.Caption = docCaption.InnerText;
        }


        // Метод соединения значений групп по дате(добавление отсутствующих дат)
        // так, чтобы для каждого отчета в группе были ИДЕНТИЧНЫЕ наборы дат
        private void _JoinReportsInGroupByDate()
        {         
            foreach (List<SlaReport> group in reportGroups.Values)
            {
                IEnumerable<SlaData> groupIntersection = group.First().GetAllData();
                foreach (SlaReport groupedReport in group)
                {
                    groupIntersection = groupIntersection.Union(groupedReport.GetAllData());
                }                
                foreach (SlaReport groupedReport in group)
                {
                    int sourceDataLen = groupedReport.GetDataSize();            

                    IEnumerable<SlaData> additionalData = 
                        from el in groupIntersection
                        where !groupedReport.GetAllData().Contains(el)
                        select new SlaData(el.Date);
                    groupedReport.MergeData(additionalData);
                    
                    // WARNING!
                    if ((float)groupedReport.GetDataSize() / sourceDataLen >= 1.5f)
                        _WriteLog(WarningType.TooMuchEmptiesAdded, groupedReport);
                }
            }
        }


        public IEnumerator<List<SlaReport>> GetEnumerator()
        {
            return reportGroups.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        // Класс, реализующий XML-узел глобальной конфигурации XLS - <global_config>
        public struct SlaDocumentConfig
        {
            private string docCaption;

            public string Caption
            {
                get { return docCaption == null ? "SLA отчетность" : docCaption; }
                set { docCaption = value; }
            }
        }

    }
}