﻿namespace SlaReportsBuilder
{
    partial class SlaBuilderMainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SlaBuilderMainForm));
            this.slaFileOpener = new System.Windows.Forms.Button();
            this.slaFilePicker = new System.Windows.Forms.OpenFileDialog();
            this.excelBuilderGroup = new System.Windows.Forms.GroupBox();
            this.shouldExcelFileBeOpened = new System.Windows.Forms.CheckBox();
            this.excelBuilderProgress = new System.Windows.Forms.ProgressBar();
            this.slaFileSaver = new System.Windows.Forms.SaveFileDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.slaStatusBar = new System.Windows.Forms.StatusStrip();
            this.slaStatusBarLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.slaStatusBarChanger = new System.Windows.Forms.Timer(this.components);
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.excelBuilderGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.slaStatusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // slaFileOpener
            // 
            this.slaFileOpener.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.slaFileOpener.Location = new System.Drawing.Point(29, 29);
            this.slaFileOpener.Name = "slaFileOpener";
            this.slaFileOpener.Size = new System.Drawing.Size(208, 33);
            this.slaFileOpener.TabIndex = 0;
            this.slaFileOpener.Text = "Выбрать файл с выгрузкой...";
            this.slaFileOpener.UseVisualStyleBackColor = true;
            this.slaFileOpener.Click += new System.EventHandler(this.slaFilePicker_Click);
            // 
            // slaFilePicker
            // 
            this.slaFilePicker.FileName = "openFileDialog1";
            // 
            // excelBuilderGroup
            // 
            this.excelBuilderGroup.Controls.Add(this.shouldExcelFileBeOpened);
            this.excelBuilderGroup.Controls.Add(this.excelBuilderProgress);
            this.excelBuilderGroup.Enabled = false;
            this.excelBuilderGroup.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.excelBuilderGroup.Location = new System.Drawing.Point(23, 82);
            this.excelBuilderGroup.Name = "excelBuilderGroup";
            this.excelBuilderGroup.Size = new System.Drawing.Size(368, 65);
            this.excelBuilderGroup.TabIndex = 4;
            this.excelBuilderGroup.TabStop = false;
            this.excelBuilderGroup.Text = "Прогресс построения отчета";
            // 
            // shouldExcelFileBeOpened
            // 
            this.shouldExcelFileBeOpened.AutoSize = true;
            this.shouldExcelFileBeOpened.Checked = true;
            this.shouldExcelFileBeOpened.CheckState = System.Windows.Forms.CheckState.Checked;
            this.shouldExcelFileBeOpened.Location = new System.Drawing.Point(6, 41);
            this.shouldExcelFileBeOpened.Name = "shouldExcelFileBeOpened";
            this.shouldExcelFileBeOpened.Size = new System.Drawing.Size(195, 19);
            this.shouldExcelFileBeOpened.TabIndex = 5;
            this.shouldExcelFileBeOpened.Text = "открыть файл по завершении";
            this.shouldExcelFileBeOpened.UseVisualStyleBackColor = true;
            // 
            // excelBuilderProgress
            // 
            this.excelBuilderProgress.Location = new System.Drawing.Point(6, 20);
            this.excelBuilderProgress.Name = "excelBuilderProgress";
            this.excelBuilderProgress.Size = new System.Drawing.Size(356, 11);
            this.excelBuilderProgress.TabIndex = 4;
            // 
            // slaFileSaver
            // 
            this.slaFileSaver.DefaultExt = "xls";
            this.slaFileSaver.Filter = "Excel (*.xls)|*.xls|All files (*.*)|*.*";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SlaReportsBuilder.Properties.Resources.vtb;
            this.pictureBox1.Location = new System.Drawing.Point(274, 29);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(117, 45);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // slaStatusBar
            // 
            this.slaStatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.slaStatusBarLabel,
            this.toolStripStatusLabel1});
            this.slaStatusBar.Location = new System.Drawing.Point(0, 174);
            this.slaStatusBar.Name = "slaStatusBar";
            this.slaStatusBar.Size = new System.Drawing.Size(415, 22);
            this.slaStatusBar.TabIndex = 6;
            // 
            // slaStatusBarLabel
            // 
            this.slaStatusBarLabel.Name = "slaStatusBarLabel";
            this.slaStatusBarLabel.Size = new System.Drawing.Size(0, 17);
            this.slaStatusBarLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // slaStatusBarChanger
            // 
            this.slaStatusBarChanger.Tick += new System.EventHandler(this.slaStatusBarChanger_Tick);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // SlaBuilderMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(415, 196);
            this.Controls.Add(this.slaStatusBar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.excelBuilderGroup);
            this.Controls.Add(this.slaFileOpener);
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SlaBuilderMainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Построитель отчетов SLA";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SlaBuilderMainForm_FormClosing);
            this.Shown += new System.EventHandler(this.SlaBuilderMainForm_Shown);
            this.excelBuilderGroup.ResumeLayout(false);
            this.excelBuilderGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.slaStatusBar.ResumeLayout(false);
            this.slaStatusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button slaFileOpener;
        private System.Windows.Forms.OpenFileDialog slaFilePicker;
        private System.Windows.Forms.GroupBox excelBuilderGroup;
        private System.Windows.Forms.ProgressBar excelBuilderProgress;
        private System.Windows.Forms.SaveFileDialog slaFileSaver;
        private System.Windows.Forms.CheckBox shouldExcelFileBeOpened;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.StatusStrip slaStatusBar;
        private System.Windows.Forms.ToolStripStatusLabel slaStatusBarLabel;
        private System.Windows.Forms.Timer slaStatusBarChanger;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}

