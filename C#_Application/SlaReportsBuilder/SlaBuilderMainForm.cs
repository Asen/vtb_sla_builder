﻿/*
 * 
 * Developer ---------- AseN
 * Public Email ------- antochi.anton@ya.ru
 * Date --------------- 26.07.16 - 30.08.16
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using SlaReportsBuilder.ExcelReport;

namespace SlaReportsBuilder
{
    public partial class SlaBuilderMainForm : Form
    {
        private enum BuildingState {Active, InActive};    // Состояния построителя

        private SlaDocument parser = null;                  // Экземпляр класса-парсера      
        private ExcelBuilder excelBuilder = null;         // Экземпляр класса отчет-билдера Excel  
        private BuildingState buildingState 
            = BuildingState.InActive;                     // Запущен ли процесс построителя?        

        public SlaBuilderMainForm()
        {
            InitializeComponent();           
        }


        private void _PrintErrorMessage(Exception exception, String title)
        {
            MessageBox.Show(exception.Message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        // Метод обработки перехваченной фатальной ошибки
        private void _ProvokeFatalError()
        {
            MessageBox.Show(
                   "Возникла ошибка. Приложение будет перезапущено.", "Ошибка",
                   MessageBoxButtons.OK, MessageBoxIcon.Error
                   );
            Application.Restart();
        }


        // Потокобезопасная установка текста в статусбар главной формы
        private void _SetUserReadableStatusMessage(string message, bool animated=false)
        {
            if (!IsHandleCreated || message==null)
                return;

            Invoke((MethodInvoker)delegate () 
            {
                slaStatusBarChanger.Enabled = false;
                slaStatusBarLabel.Text = message;
                slaStatusBarChanger.Enabled = animated;
            });
        }


        // Проверка, не запущен ли уже процесс Excel-билдера
        private bool _IsBuildingProcessRunning()
        {
            return buildingState == BuildingState.Active;
        }


        // Потокобезопасное выполнение действий, предшествующих процессу Excel-построения 
        private void _PrepareToBuilding()
        {
            if (!IsHandleCreated)
            {
                _ProvokeFatalError();
                return;
            }           

            Invoke((MethodInvoker)delegate ()
            {                
                buildingState = BuildingState.Active;
               
                slaFileOpener.Enabled = false;
                excelBuilderGroup.Enabled = true;
                _SetUserReadableStatusMessage("Идет процесс чтения файла выгрузки...", true);
                excelBuilderProgress.Value = 2;
            });                     
        }


        // Потокобезопасное выполнение действий по завершении процесса Excel-построения
        private void _FinishBuilding()
        {
            if (!IsHandleCreated)
            {
                _ProvokeFatalError();
                return;
            }

            Invoke((MethodInvoker)delegate ()
            {
                buildingState = BuildingState.InActive;

                slaFileOpener.Enabled = true;
                excelBuilderGroup.Enabled = false;
                _SetUserReadableStatusMessage("Построитель готов к построению отчетов.");
                excelBuilderProgress.Value = 0;
            });                     
        }


        private void slaFilePicker_Click(object sender, EventArgs e)
        {

            if (slaFilePicker.ShowDialog() == DialogResult.OK)
            {

                _PrepareToBuilding();

                try
                {
                    // Попытка парсинга выгрузки SLA из "Менеджера отчетов"          
                    parser = new SlaDocument(slaFilePicker.FileName);
                    parser.Parse();
                    // вывод сообщений парсера
                    if (!parser.IsLogEmpty())
                    {
                        DialogResult dialRes = MessageBox.Show(parser.GetWarningsLog() + "\n\n\nВсе равно построить графики?", "Предупреждение", 
                            MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                        if (dialRes == DialogResult.No)
                        {
                            _FinishBuilding();
                            return;
                        }
                    }
                }
                catch(Exception parseException)
                {
                    _FinishBuilding();
                    _SetUserReadableStatusMessage("Произошла ошибка.");
                    _PrintErrorMessage(parseException, "Ошибка на этапе чтения файла выгрузки");                  
                    return;
                }

                // Стандартный путь для сохранения обработанной выгрузки - в ту же директорию:
                string outputFile = Path.Combine(Path.GetDirectoryName(slaFilePicker.FileName), "SLA.xls");

                // Хочет ли юзер изменить директорию по умолчанию?
                DialogResult outputSaveDialog = MessageBox.Show(
                    "Изменить место, куда сохранить выходной файл с графиками?", "Место сохранения Excel файла",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1, (MessageBoxOptions)0x40000);

                // Меняем директорию 
                if (outputSaveDialog == DialogResult.Yes)
                    if (slaFileSaver.ShowDialog() == DialogResult.OK)
                        outputFile = slaFileSaver.FileName;

                // Запуск процесса Excel-построителя                
                _BuildExcelOutput(outputFile);        
            }
        }


        // Построение графиков отчетов в формате Excel в параллельном потоке
        private void _BuildExcelOutput(string outputFile)
        {         
            new Thread(delegate () {

                //Stopwatch timing = new Stopwatch();
                //timing.Start();

                try
                {
                    _SetUserReadableStatusMessage("Подготавливается Excel-файл (может занять некоторое время)...", true);
                    excelBuilder = new ExcelBuilder(parser, outputFile);
                    _SetUserReadableStatusMessage("Идет процесс построения SLA-диаграмм...", true);
                    excelBuilder.onProgress += ExcelBuilder_onProgress;
                    excelBuilder.Build();
                }
                catch (Exception buildException)
                {
                    excelBuilder.Dispose();
                    _FinishBuilding();
                    _SetUserReadableStatusMessage("Произошла ошибка.");
                    _PrintErrorMessage(buildException, "Ошибка на этапе создания XLS-файла выгрузки");                                  
                    return;
                }

                //timing.Stop();
                //double elapsedSeconds = Math.Round(timing.ElapsedMilliseconds / 1000d, 1);
                string mboxFname = outputFile.Substring(0, Math.Min(128, outputFile.Length));
                if (mboxFname.Length > 64)
                    mboxFname = mboxFname.Insert(64, "\n");
                //_SetUserReadableStatusMessage("Отчет успешно построен за "+elapsedSeconds+" cек. Файл сохранен: \n" + mboxFname);
                _SetUserReadableStatusMessage("Отчет успешно построен. Файл с отчетом сохранен как: \n" + mboxFname);

                _FinishBuilding();

                // Открытие XLS-файла по окончании процесса
                if (shouldExcelFileBeOpened.Checked && File.Exists(outputFile))
                    Process.Start(outputFile);

            }).Start();
        }


        // Обработчик события прогресса из Excel-билдера (строго в UI-потоке)
        private void ExcelBuilder_onProgress(float progress)
        {
            // Если юзеру каким-то образом удалось убить UI-поток
            if (!IsHandleCreated)
                return;

            Invoke((MethodInvoker)delegate ()
            {
                excelBuilderProgress.Value = (int)(progress * 100.0);  
            });
        }


        // Безопасное закрытие приложения с учетом многопоточности
        private void SlaBuilderMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_IsBuildingProcessRunning())
            {
                DialogResult dialRes = MessageBox.Show(
                    "Идет процесс построения отчета. Остановить процесс?", "Процесс построения будет завершен",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning
                    );
                if (dialRes == DialogResult.Yes)
                    Process.GetCurrentProcess().Kill();
                else
                    e.Cancel = true;        
            }
        }


        private void SlaBuilderMainForm_Shown(object sender, EventArgs e)
        {
            _SetUserReadableStatusMessage("Построитель готов к построению отчетов.");
        }


        private void slaStatusBarChanger_Tick(object sender, EventArgs e)
        {
            char symbol = '.';
            string finalEnd = new String(symbol, 10);

            string statusText = slaStatusBarLabel.Text.Trim();
            if (statusText.EndsWith(finalEnd))
                slaStatusBarLabel.Text = statusText.Replace(finalEnd, "");
            else
                slaStatusBarLabel.Text += symbol;
        }
    }
}
