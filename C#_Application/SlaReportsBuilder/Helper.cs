﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace SlaReportsBuilder
{
    public static class Helper
    {
        /* Преобразование (String -> Double) с учетом особенностей локали */
        public static bool CulturalStrToFloat(string str, out float result)
        {
            // Чистка форматирования числа PLSQL`ом
            str = str.Replace(',', '.').Replace(" ","").Trim();

            return
            float.TryParse(
                   str,
                   System.Globalization.NumberStyles.Any,
                   CultureInfo.GetCultureInfo("en-US"),
                   out result
                   );
        }


        /* Аккуратная разбивка длинной строки на множество подстрок */
        public static string NeatSplit(this string str, int maxLineLen)
        {
            char[] splitters = new char[] { ' ', '\r', '\n' };

            string[] words = str.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
            StringBuilder result = new StringBuilder();
            StringBuilder line = new StringBuilder();
            foreach (string word in words)
            {
                if ((line.ToString() + word).Length >= maxLineLen)
                {
                    if (line.ToString().Trim().Length > 0)
                    {
                        result.Append(line);
                        result.Append("\n");
                    }
                    line.Clear();
                }
                line.Append(word);
                line.Append(" ");
            }

            if (line.Length > 0)
                result.Append(line);

            return result.ToString().Trim(splitters);
        }


        /* Выделение названия метрики */
        public static string ExtractGroupName(this SlaReport report)
        {
            int reportNameCommaPos = report.GetFullName().IndexOf('[');
            return 
                reportNameCommaPos >= 0 ?
                report.GetFullName().Substring(0, reportNameCommaPos).Trim() :
                report.GetFullName();
        }


        /* Получение нормализированного имени группы для определения уникальности */
        public static string GenGroupNameCode(this SlaReport report)
        {
            string normalizedStr = report.ExtractGroupName().ToLower().Trim();
            return string.Join(" ", normalizedStr.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
        } 


        /* Выделение конкретики из названия метрики(то, что в скобках названия) */
        public static string ExtrctReportSpec(this SlaReport report)
        {
            int reportStartCommaPos = report.GetFullName().IndexOf('[');
            int reportEndCommaPos = report.GetFullName().LastIndexOf(']');
            return 
                reportEndCommaPos > reportStartCommaPos ?
                report.GetFullName().Substring(reportStartCommaPos + 1, reportEndCommaPos - reportStartCommaPos - 1).Trim() :
                string.Empty;
        }

    }
}
