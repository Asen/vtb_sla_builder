﻿using System;
using System.Runtime.Serialization;

namespace SlaReportsBuilder
{
    class SlaHandledException : Exception
    {
        public  ErrorCode Code;
        private string specification = null;

        public SlaHandledException(ErrorCode Code)
        {
            this.Code = Code;
        }

        public SlaHandledException(ErrorCode Code, string specification) : this(Code)
        {
            this.specification = specification;
        }

        public SlaHandledException(ErrorCode Code, Exception decorException) : this(Code)
        {
            this.specification = decorException.Message;
        }

        public override string Message
        {
            get
            {
                return Code.GetMessage() + (specification != null ? "\n\n---\n" + specification : "");
            }
        }

    }

    public class ErrorCode
    {
        public static ErrorCode XmlNoRootElementFound =
            new ErrorCode(
                "Выбранный файл не является корректным файлом выгрузки.\n" +
                "Убедитесь, что входной файл имеет корневой элемент <sla>, в который вложен <reports>\n" +
                "Также убедитесь, что входной файл имеет кодировку Windows-1251 (cp1251)"
                );
        public static ErrorCode XmlReportKeyDuplicate =
            new ErrorCode(
                "ID отчета пересекается с ID другого отчета.\n"+
                "Дубликаты кодов отчетов недопустимы.\n"+
                "Выберите любое другое уникальное число."
                );
        public static ErrorCode XmlNoReportInfoElementFound =
            new ErrorCode(
                "В файле выгрузки для одного или нескольких отчетов отсутствуют информационные узлы SLA"
                );
        public static ErrorCode XmlDigitConversion =
            new ErrorCode(
                 "Файл выгрузки содержит некорректные данные в одном или нескольких числовых узлах.\n" +
                 "Не удалось преобразовать значени поля к числу"
                );
        public static ErrorCode XmlNoDataElementFound =
            new ErrorCode(
                 "Для одного или нескольких SLA-отчетов отсутствует элемент с набором данных <data>"
                );
        public static ErrorCode XmlNoDateAttributeFound =
            new ErrorCode(
                "Для одного или нескольких узлов <measure> отсутствует атрибут 'date'"
                );
        public static ErrorCode XmlDateConversion =
            new ErrorCode(
                 "В одном или нескольких атрибутах 'date' узла <measure> содержится дата в некорректном формате\n" +
                 "Правильный формат даты: \"dd.mm.yyyy\""
                );
        public static ErrorCode XlsWriteFileNoAccess =
            new ErrorCode(
                "Не удалось сохранить результат в XLS файл.\n" +
                "Возможно, у приложения не хватает прав, либо этот файл открыт и используется в данный момент.\n" +
                "В таком случае закройте этот файл и перезапустите построитель."
                );
        public static ErrorCode XlsChartTooLargeHeight =
            new ErrorCode(
                "Размеры одной или нескольких диаграмм не удается разместить на странице формата A4.\n"+
                "Для устранения ошибки убедитесь, что сгенерированные диаграммы по высоте не превышают 90 ячеек."
                );

        private string Message = null;

        private ErrorCode(string error_msg)
        {
            Message = error_msg;
        }

        public string GetMessage()
        {
            return Message;
        }
    }

}
