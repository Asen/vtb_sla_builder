﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlaReportsBuilder
{
    /* Класс, описывающий XML-объект "report" */
    public class SlaReport
    {
        private enum ChartType { ChartLine, ChartGistogramm };
       
        private int     sla_id;
        private string  sla_report_name = null;
        private float   sla_std_measure = -1;
        private string  sla_measure_unit = null;
        private ChartType type = ChartType.ChartLine;
        private SortedSet<SlaData> Data;

        public SlaReport(int id, string name, float stdMeasure, string stdMeasureUnit){
            sla_id = id;
            sla_report_name = name;
            sla_std_measure = (stdMeasure >= 0 && !float.IsNaN(stdMeasure)) ? stdMeasure : -1;
            sla_measure_unit = stdMeasureUnit;
            Data = new SortedSet<SlaData>();
        }
        
        public string GetFullName() { return sla_report_name.Trim(); }
        public float GetSlaMeasure() { return sla_std_measure; }
        public string GetMeasureUnit() { return sla_measure_unit.Trim(); }

        public IEnumerable<SlaData> GetAllData() { return Data; }
        public int GetDataSize() { return Data.Count;  }
        public void AppendData(SlaData measure){  Data.Add(measure);  }
        public void MergeData(IEnumerable<SlaData> colelction) { Data.UnionWith(colelction);  }
        public bool Empty(){ return Data.All(row => row.Empty); }

        public bool HasSlaLine(){ return sla_std_measure >= 0; }
        public void SetChartType(string chartId)
        {
            if (chartId == null || chartId.Equals(string.Empty))
                type = ChartType.ChartLine;
            if (chartId.Trim().ToLower().Equals("gist"))
                type = ChartType.ChartGistogramm;
        }
        public bool IsChartGistogrammic() { return type == ChartType.ChartGistogramm; } 
        public override int GetHashCode()
        {
            return sla_id;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            return GetHashCode() == ((SlaReport)obj).GetHashCode();
        }
    }

    /* Подкласс, описывающий XML-подобъект "measure" */
    public class SlaData : IComparable
    {      
        private DateTime date;        // Ключ, опеределяющий уникальность всего объекта
        private float  measure;
        private bool isEmpty;         // Флаг, указывающий на то, что это dummy-объект(пустышка)
                                      // (пустышка - если не заполнено поле "measure")

        public DateTime Date
        {
            get { return date; }
        }

        public float Measure
        {
            get { return Empty ? -1 : measure; }
        }

        public bool Empty
        {
            get { return isEmpty; }
            set { isEmpty = value; }
        }

        public SlaData(DateTime _date, float _measure)
        {
            date = new DateTime(_date.Year, _date.Month, _date.Day);
            measure = _measure;
            Empty = false;
        }

        public SlaData(DateTime _date)
        {
            date = new DateTime(_date.Year, _date.Month, _date.Day);
            Empty = true;
        }

        public override int GetHashCode()
        {
            return date.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            return GetHashCode() == ((SlaData)obj).GetHashCode();
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
                return -1;

            return date.CompareTo(((SlaData)obj).date);
        }
    }
}