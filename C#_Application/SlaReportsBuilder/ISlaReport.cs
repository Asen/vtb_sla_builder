﻿using System;

namespace SlaReportsBuilder
{
    // Контракт для создания нового генератора SLA-отчета
    public interface ISlaReportBuilder : IDisposable
    {
        event Action<float> onProgress;
        void Build();
    }
}
