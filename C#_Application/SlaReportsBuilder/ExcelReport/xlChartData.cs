﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;

namespace SlaReportsBuilder.ExcelReport
{
    public partial class ExcelBuilder
    {
        // Класс для аллокации информации о построении конкретной диаграммы на XLS-листе
        private class xlChartData
        {
            // Диапазон X-значений
            private Excel.Range XValues = null;
            // Диапазоны Y-значений, связанные с объектами SlaReport
            private Dictionary<SlaReport, Excel.Range> Series = new Dictionary<SlaReport, Excel.Range>();
            // Диапазоны SLA-линий
            private List<Excel.Range> Sla = new List<Excel.Range>();


            public void SetXValues(Excel.Range xlRange)
            {
                XValues = xlRange;
            }
            public void AddSeries(Excel.Range xlRange, SlaReport report)
            {
                Series.Add(report, xlRange);
            }
            public void AddSlaLine(Excel.Range xlRange)
            {
                Sla.Add(xlRange);
            }
            public Excel.XlChartType GetXlChartType()
            {
                foreach (SlaReport report in Series.Keys)
                {
                    if (report.IsChartGistogrammic())
                        return Excel.XlChartType.xlColumnClustered;
                }
                return Excel.XlChartType.xlLineMarkers;
            }
            public string GetChartTitle()
            {
                SlaReport sample = Series.Keys.Where(r => r.ExtractGroupName() != null && r.ExtractGroupName().Length > 0).FirstOrDefault();
                return sample == null ? "???" : sample.ExtractGroupName();
            }
            public string GetYAxisName()
            {
                SlaReport sample = Series.Keys.Where(r => r.GetMeasureUnit() != null && r.GetMeasureUnit().Length > 0).FirstOrDefault();
                return sample == null ? "???" : sample.GetMeasureUnit();
            }
            public Excel.Range GetXValues()
            {
                return XValues;
            }
            public IEnumerable<KeyValuePair<SlaReport, Excel.Range>> GetYValues()
            {
                return Series;
            }
            public IEnumerable<Excel.Range> GetSlaLines()
            {
                return Sla;
            }
        }
    }
}
