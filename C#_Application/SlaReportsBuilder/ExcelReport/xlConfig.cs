﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace SlaReportsBuilder.ExcelReport
{
    public partial class ExcelBuilder
    {
        // Класс с конфигурацией UI в формате Excel
        private static class xlConfig
        {
            // Размеры ячейки Excel
            public const int xlCellWidth = 48; //px
            public const int xlCellHeight = 15; //px

            // Глобальный шрифт XLS-файла
            public const string xlFontName = "Arial";

            // Размеры графиков
            public const int chartWidthPx = 18 * xlCellWidth;
            public const int chartHeightPx = 35 * xlCellHeight;
            public const int chartsMinBetweenOffsetPx = 2 * xlCellHeight;

            // Количество ячеек(по высоте) на страницу формата А4 на основе ширины листа(ширины графиков)
            public const int pageA4MaxRows = (int)(chartWidthPx / xlCellWidth * 5.3f);

            // Цветовая схема графиков
            public static readonly int chartFontColor = ColorTranslator.ToOle(Color.FromArgb(10, 41, 115));
            public static readonly int chartBackgroundColor = ColorTranslator.ToOle(Color.FromArgb(255, 255, 255));
            public static readonly int chartSeriesSlaColor = ColorTranslator.ToOle(Color.OrangeRed);

            // Цвета, используемые при рисовании серий графика
            public static readonly int[] chartSeriesColors = {
                ColorTranslator.ToOle(Color.FromArgb(70,130,180)),
                ColorTranslator.ToOle(Color.FromArgb(146,205,81))
            };

            // Формат, в котором выводить дату на графике (по OX)
            public const string chartDateFmt = "dd/MM/yyyy";
            // Фон страницы с графиками
            public static readonly int pageBackgroundColor = chartBackgroundColor;

            // Параметры хедера страницы с графиками
            public const int headerHeightPx = 5 * xlCellHeight;
            public const int vtbLogoHeightPx = 50;                            // высота изображения на XLS
            public const int vtbLogoWidthPx = (int)(vtbLogoHeightPx * 2.58f); // соотношение сторон изображения
            public const int vtbLogoYOffsetPx = 5;
            public const int vtbLogoXRightOffsetpx = 10;
            public static readonly int decorHeaderBackgroundStart = chartFontColor;
            public static readonly int decorHeaderBackgroundEnd = pageBackgroundColor;
            public static readonly int decorHeaderFontColor = chartBackgroundColor;

            // Параметры футера страницы с графиками
            public const int footerHeightPx = 5 * xlCellHeight;
            public static readonly int decorFooterBackgroundStart = decorHeaderBackgroundStart;
            public static readonly int decorFooterBackgroundEnd = decorHeaderBackgroundEnd;

        }
    }
}
