﻿using Microsoft.Office.Core;
using SlaReportsBuilder.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace SlaReportsBuilder
{
    namespace ExcelReport
    {
        public partial class ExcelBuilder : ISlaReportBuilder
        {
            private SlaDocument slaDoc = null;
            private int progress = 0;
            private string outFileName = null;

            private object xlNULL = System.Reflection.Missing.Value;
            private Excel.Application xlApp = null;
            private Excel.Workbook xlWorkBook = null;
            private Excel.Workbook xlStaticWorkbook = null;
            private Excel.Worksheet xlSheetData = null;
            private Excel.Worksheet xlSheetCharts = null;

            // Событие прогресса в построении отчета
            public event Action<float> onProgress = null;
            private readonly int totalPossibleProgressEvents;

            public ExcelBuilder(SlaDocument parseData, string fileToSave)
            {
                _InitExcel();
                var staticSheets = xlStaticWorkbook.Sheets;

                slaDoc = parseData;
                outFileName = fileToSave;
                totalPossibleProgressEvents = (3 * slaDoc.Count()) + staticSheets.Count;
            }


            // Событие прогресса в построении 
            // Вызывается:
            //      1)при добавлении данных на страницу  из   _FillDataSheet()     --> slaDoc.Count() раз 
            //      2)при добавлении нового графика      из   _FillGraphicsSheet() --> slaDoc.Count() раз
            //      3)при генерации печатаемой страницы  из   _GeneratePage()      --> slaDoc.Count() + static.Length
            private void _OnProgress()
            {
                progress++;
                float buildingProgress = (float)progress / totalPossibleProgressEvents;

                if (onProgress != null)
                    onProgress.Invoke(buildingProgress);
            }


            // Построение содержимого XLS-файла
            public void Build()
            {
                // Буфер данных, содержащий информацию для построения графиков 
                List<xlChartData> chartsData = _FillDataSheet();
                // Загрузка построенных диаграмм в буфер Excel-объектов
                List<xlObjectArea> objectsToPrint = _FillGraphicsSheet(chartsData);
                // Обогащение буфера Excel-объектов статикой
                objectsToPrint.AddRange(_GetStaticObjects());

                // Количество групп(диаграмм), которое вмещается на страницу A4 с учетом высот хедера/футера
                int chartsNumberOnPage = _CalcChartsNumberOnPrintablePage();
                // Всего страниц планируется сгенерировать
                int pagesTotal = (int)Math.Ceiling(objectsToPrint.Count / (double)chartsNumberOnPage);

                // Генерация страниц
                for (int pageNum = 0; pageNum < pagesTotal; pageNum++)
                {
                    int builtGroups = pageNum * chartsNumberOnPage;
                    var dataGroups = objectsToPrint.Skip(builtGroups).Take(chartsNumberOnPage).ToList();
                    _GeneratePage(pageNum, dataGroups);
                }

                _SaveToFile();
                Dispose();
            }

            // Действия, совершаемые при завершении работы билдера
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            protected virtual void Dispose(bool backgroundDisposal)
            {
                if (xlWorkBook != null) xlWorkBook.Close(0);
                if (xlStaticWorkbook != null) xlStaticWorkbook.Close(0);
                if (xlApp != null) xlApp.Quit();
                slaDoc = null;

                // Очистка RCW оболочек COM окружения
                Marshal.FinalReleaseComObject(xlSheetData);
                Marshal.FinalReleaseComObject(xlSheetCharts);
                Marshal.FinalReleaseComObject(xlWorkBook);
                Marshal.FinalReleaseComObject(xlStaticWorkbook);
                Marshal.FinalReleaseComObject(xlApp);
                GC.Collect();
            }

            // Метод инициализации Excel-файла. Может выполняться достаточно долго
            // на компьютере с загруженной памятью/CPU. Выполняется быстрее, если Excel уже открыт.
            private void _InitExcel()
            {
                xlApp = new Excel.Application();
                xlApp.DisplayAlerts = false;

                var xlAppWorkbooks = xlApp.Workbooks;
                xlWorkBook = xlAppWorkbooks.Add(xlNULL);
                xlWorkBook.CheckCompatibility = false;

                // Страница с данными
                xlSheetData = (Excel.Worksheet)xlWorkBook.ActiveSheet;
                xlSheetData.Name = "Данные";
                var sheetCells = xlSheetData.Cells;
                var sheetCellsFont = sheetCells.Font;
                sheetCellsFont.Name = xlConfig.xlFontName;

                // Страница с визуальным представлением
                xlSheetCharts = (Excel.Worksheet)xlWorkBook.Worksheets.Add(xlNULL, xlNULL, 1, xlNULL);
                xlSheetCharts.Name = "Графики";
                sheetCells = xlSheetCharts.Cells;
                sheetCellsFont = sheetCells.Font;
                sheetCellsFont.Name = xlConfig.xlFontName;

                // Получение внешнего WorkBook`а со статичными объектами Excel
                string pathToStatics = Path.Combine(Program.GetAppPath(), "StaticData.xlsx");
                // если внешнего файла не существует на данный момент, то взять его из ресурсов
                if (!File.Exists(pathToStatics))
                    File.WriteAllBytes(pathToStatics, Resources.excel_static_data);
                xlStaticWorkbook = xlAppWorkbooks.Open(pathToStatics);

                // Ячейка, на основе которой будет производиться расчет размеров всех остальных
                Excel.Range stdRange = xlSheetCharts.Range["A1"];
                var sheetRows = xlSheetCharts.Rows;

                // Установка нужной высоты строк на странице
                float deltaHeight = 0.1f;
                stdRange.RowHeight = 0;
                while (stdRange.Height < xlConfig.xlCellHeight)
                {
                    double pxHeight = stdRange.Height;
                    stdRange.RowHeight += deltaHeight;
                    if (pxHeight >= stdRange.Height) deltaHeight += 0.1f;
                }          
                sheetRows.RowHeight = stdRange.RowHeight;
            
                // Установка нужной ширины столбцов на странице
                float deltaWidth = 0.1f;
                stdRange.ColumnWidth = 0;
                while (stdRange.Width < xlConfig.xlCellWidth)
                {
                    double pxWidth = stdRange.Width;
                    stdRange.ColumnWidth += deltaWidth;
                    if (pxWidth >= stdRange.Width) deltaWidth += 0.1f;
                }
                sheetRows.ColumnWidth = stdRange.ColumnWidth;

                // Настройка дефолтных параметров страницы печати
                Excel.PageSetup pgSetup = xlSheetCharts.PageSetup;
                pgSetup.PaperSize = Excel.XlPaperSize.xlPaperA4;
                pgSetup.TopMargin = 54;
                pgSetup.BottomMargin = 54;
                pgSetup.LeftMargin = 50;
                pgSetup.RightMargin = 70;
                pgSetup.Zoom = 50;
            }


            // Получение объектов из файла-статики
            private List<xlObjectArea> _GetStaticObjects()
            {
                List<xlObjectArea> staticObjs = new List<xlObjectArea>();

                foreach (Excel.Worksheet sheet in xlStaticWorkbook.Sheets)
                    staticObjs.Add(new xlObjectArea(sheet, 1, 1));

                return staticObjs;
            }


            // Генерация страницы с графиками, приспособленной для удобного импорта в формате листов A4
            private void _GeneratePage(int pageNumber, List<xlObjectArea> objects)
            {
                int nextOffsetYpx = pageNumber * xlConfig.pageA4MaxRows * xlConfig.xlCellHeight;
                int pageStart = nextOffsetYpx;
                int pageFreeSpacePx =
                    xlConfig.pageA4MaxRows * xlConfig.xlCellHeight -
                    (xlConfig.headerHeightPx + xlConfig.footerHeightPx + xlConfig.chartHeightPx * objects.Count);
                int totalSpaces = 2 + objects.Count - 1;
                int avgSpaceSizePx = pageFreeSpacePx / totalSpaces;

                // Добавление заголовка на страницу
                int headerYCell = nextOffsetYpx / xlConfig.xlCellHeight + 1;
                _InsertPageHeader(headerYCell);

                nextOffsetYpx += xlConfig.headerHeightPx;

                // Добавление на страницу отчетов, равномерно распределяя пространство по высоте между ними
                foreach (xlObjectArea xlObject in objects)
                {
                    nextOffsetYpx += avgSpaceSizePx;
                    pageFreeSpacePx -= avgSpaceSizePx;

                    int cellY = (nextOffsetYpx + (xlConfig.xlCellHeight - nextOffsetYpx % xlConfig.xlCellHeight)) / xlConfig.xlCellHeight;
                    xlObject.ReplaceTo(new xlObjectArea(xlSheetCharts, 1, cellY));

                    // Фиксация прогресса
                    _OnProgress();

                    nextOffsetYpx += xlConfig.chartHeightPx;
                }

                nextOffsetYpx += pageFreeSpacePx;
                pageFreeSpacePx = 0;

                // Добавление футера на страницу
                int footerYCell = nextOffsetYpx / xlConfig.xlCellHeight + 1;
                _InsertPageFooter(pageNumber + 1, footerYCell);

                // Заливка страницы            
                int yBgStart = (pageStart + xlConfig.headerHeightPx) / xlConfig.xlCellHeight + 1;
                int yBgEnd = yBgStart + xlConfig.pageA4MaxRows - (xlConfig.headerHeightPx + xlConfig.footerHeightPx) / xlConfig.xlCellHeight - 1;
                int xBgEnd = xlConfig.chartWidthPx / xlConfig.xlCellWidth;
                Excel.Range bgStart = (Excel.Range)xlSheetCharts.Cells[yBgStart, 1];
                Excel.Range bgEnd = (Excel.Range)xlSheetCharts.Cells[yBgEnd, xBgEnd];
                Excel.Range background = xlSheetCharts.get_Range(bgStart, bgEnd);
                background.Interior.Color = xlConfig.pageBackgroundColor;

                // Добавление разрыва страницы в самый низ (для удобной печати в формате А4)   
                int breakYCell = (pageNumber + 1) * xlConfig.pageA4MaxRows + 1;
                xlSheetCharts.HPageBreaks.Add((xlSheetCharts.Range["A" + breakYCell, "A" + breakYCell].EntireRow));
            }


            // Метод, заполняющий страницу с данными и возвращающий буфер с данными для построения графиков
            private List<xlChartData> _FillDataSheet()
            {
                int dataOffsetX = 1;                                         // Смещение от левого края XLS-листа
                List<xlChartData> dataSheetGroups = new List<xlChartData>(); // Буфер данных

                foreach (List<SlaReport> group in slaDoc)
                {
                    int initialX = dataOffsetX;
                    int groupDataNum = group[0].GetDataSize();
                    string groupName = group[0].ExtractGroupName();


                    xlChartData dataGroup = new xlChartData();    // Объект для буфера данных           


                    foreach (SlaReport report in group)
                    {
                        // Заголовки столбцов группы
                        string reportSpec = report.ExtrctReportSpec();
                        xlSheetData.Cells[2, dataOffsetX + 0] = "Дата";
                        xlSheetData.Cells[2, dataOffsetX + 1] = (reportSpec == null) ? "Значение" : reportSpec;

                        // Заполнение столбцов данными
                        for (int i = 0; i < report.GetDataSize(); i++)
                        {
                            SlaData point = report.GetAllData().ElementAt(i);
                            xlSheetData.Cells[i + 3, dataOffsetX + 0] = point.Date.ToString(xlConfig.chartDateFmt);

                            if (point.Empty)
                                xlSheetData.Cells[i + 3, dataOffsetX + 1] = "=NA()";  // #Н/Д
                            else
                                xlSheetData.Cells[i + 3, dataOffsetX + 1] = Math.Round(point.Measure, 2);
                        }

                        // Заполнение объекта буфера X-значениями
                        Excel.Range XValues = xlSheetData.get_Range(
                            (Excel.Range)xlSheetData.Cells[3, dataOffsetX],
                            (Excel.Range)xlSheetData.Cells[3 + groupDataNum - 1, dataOffsetX]);
                        dataGroup.SetXValues(XValues);
                        // Заполнение объекта буфера Y-значениями
                        Excel.Range YValues = xlSheetData.get_Range(
                            (Excel.Range)xlSheetData.Cells[3, dataOffsetX + 1],
                            (Excel.Range)xlSheetData.Cells[3 + groupDataNum - 1, dataOffsetX + 1]);
                        dataGroup.AddSeries(YValues, report);

                        dataOffsetX += 2;
                    }

                    // Сбор не повторяющихся показателей SLA среди дефолтных
                    float[] uniqSLA = group.Where(el => el.HasSlaLine()).Select(el => el.GetSlaMeasure()).Distinct().ToArray();
                    // Обработка всех дефолтных покзателей SLA
                    for (int uniqSlaIdx = 0; uniqSlaIdx < uniqSLA.Length; uniqSlaIdx++)
                    {
                        // Заголовок очередного SLA-столбца
                        xlSheetData.Cells[2, dataOffsetX + 0] = "SLA" + (uniqSLA.Length > 1 ? (uniqSlaIdx + 1) + "" : "");

                        // Добавление очередной серии к графику на основе столбцов, заполненных выше 
                        Excel.Range slaRange = xlSheetData.get_Range(
                                (Excel.Range)xlSheetData.Cells[3, dataOffsetX],
                                (Excel.Range)xlSheetData.Cells[3 + Math.Max(2, groupDataNum) - 1, dataOffsetX]);

                        // Заполнение столбца данными (хотя бы 2 точки)
                        slaRange.Value = uniqSLA[uniqSlaIdx];

                        // Добавление линии SLA в объект буфера
                        dataGroup.AddSlaLine(slaRange);

                        dataOffsetX++;
                    }

                    // Заголовок группы SLA-метрики
                    xlSheetData.Range[xlSheetData.Cells[1, initialX], xlSheetData.Cells[1, dataOffsetX - 1]].Merge();
                    xlSheetData.Cells[1, initialX] = groupName;
                    // Стиль заголовка
                    var lightBlueColor = ColorTranslator.ToOle(Color.FromArgb(173, 216, 230));
                    var groupCaptionInterior = ((Excel.Range)xlSheetData.Cells[1, initialX]).Interior;
                    groupCaptionInterior.Color = lightBlueColor;
                    // Стиль столбца-разделителя
                    var separatorColumn = ((Excel.Range)xlSheetData.Cells[1, dataOffsetX]).EntireColumn;
                    var separatorColumnInterior = separatorColumn.Interior;
                    separatorColumnInterior.Color = lightBlueColor;

                    dataOffsetX += 1;

                    // Добавление наполненного объекта данных в буфер
                    dataSheetGroups.Add(dataGroup);

                    // Фиксация прогресса
                    _OnProgress();

                }

                return dataSheetGroups;
            }


            // Построение диаграмм на листе во врЕменном месте
            private List<xlObjectArea> _FillGraphicsSheet(List<xlChartData> chartsData)
            {
                List<xlObjectArea> charts = new List<xlObjectArea>();

                int x = xlConfig.xlCellWidth * 100;
                int y = xlConfig.xlCellHeight * 1;

                // Добавление на страницу отчетов, равномерно распределяя пространство по высоте между ними
                foreach (xlChartData groupedReportsData in chartsData)
                {
                    // Создание каждой диаграммы на странице
                    Excel.Chart chart = _CreateNewChartGroup(x, y, groupedReportsData);
                    _FillChartWithSeries(chart, groupedReportsData);
                    _FillChartWithSlaLines(chart, groupedReportsData);

                    int cellX = 1 + x / xlConfig.xlCellWidth;
                    int cellY = 1 + y / xlConfig.xlCellHeight;
                    charts.Add(new xlObjectArea(xlSheetCharts, cellX, cellY));

                    y += xlConfig.chartHeightPx;

                    // Фиксация прогресса
                    _OnProgress();
                }

                return charts;
            }

            // Добавление новой группы визуального представления
            private Excel.Chart _CreateNewChartGroup(int pxX, int pxY, xlChartData data)
            {
                string chartTitle = data.GetChartTitle();
                string yTitle = data.GetYAxisName();
                Excel.XlChartType chartType = data.GetXlChartType();

                // Вставка группы в XLS-лист           
                int printableWidthShrunkPx = 3; // чтобы рамки не вылезали на другие страницы при печати
                Excel.ChartObjects xlCharts = (Excel.ChartObjects)xlSheetCharts.ChartObjects(Type.Missing);
                Excel.ChartObject groupChart =
                    xlCharts.Add(pxX, pxY, xlConfig.chartWidthPx - printableWidthShrunkPx, xlConfig.chartHeightPx);

                Excel.Chart chart = groupChart.Chart;
                chart.ChartType = chartType;
                //chart.ApplyLayout(1);
                chart.ChartArea.Font.Name = xlConfig.xlFontName;
                chart.HasTitle = true;
                chart.ChartTitle.Text = chartTitle;
                chart.ChartTitle.Font.Color = xlConfig.chartFontColor;
                chart.ChartArea.Border.Color = xlConfig.chartFontColor;
                // Убрать границу у плота
                chart.PlotArea.Border.LineStyle = Excel.XlLineStyle.xlLineStyleNone;

                groupChart.Interior.Color = xlConfig.chartBackgroundColor;
                chart.PlotArea.Interior.Color = xlConfig.chartBackgroundColor;

                var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
                yAxis.HasTitle = true;
                yAxis.AxisTitle.Text = yTitle;
                yAxis.AxisTitle.Font.Color = xlConfig.chartFontColor;
                yAxis.AxisTitle.Font.Bold = false;
                yAxis.AxisTitle.Font.Size = 14;
                yAxis.TickLabels.Font.Color = xlConfig.chartFontColor;
                // Формат сетки графика по OY (бэкграунд-сетка)
                yAxis.MajorGridlines.Border.Color = xlConfig.chartFontColor;
                yAxis.MajorGridlines.Border.LineStyle = Excel.XlLineStyle.xlDash;
                yAxis.Border.Color = xlConfig.chartFontColor;

                var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
                xAxis.TickLabels.Font.Color = xlConfig.chartFontColor;
                xAxis.Border.Color = xlConfig.chartFontColor;
                // Формат сетки графика по OX (бэкграунд-сетка)
                xAxis.MajorGridlines.Border.Color = xlConfig.chartFontColor;
                xAxis.MajorGridlines.Border.LineStyle = Excel.XlLineStyle.xlDash;

                // Специфичные для типа диаграммы настройки
                if (chartType == Excel.XlChartType.xlLineMarkers)
                {
                    // Точки строго по точкам оси OX, а не на промежутках
                    xAxis.AxisBetweenCategories = false;
                }

                // Формат легенды            
                chart.Legend.Font.Color = xlConfig.chartFontColor;
                chart.Legend.Position = Excel.XlLegendPosition.xlLegendPositionRight;

                return chart;
            }


            // Добавление серии данных отчета в группу
            private void _FillChartWithSeries(Excel.Chart chart, xlChartData data)
            {
                foreach (KeyValuePair<SlaReport, Excel.Range> series in data.GetYValues())
                {
                    SlaReport originalReport = series.Key;

                    // Если отчет пуст(нет точек на графике), то его следует пропустить
                    if (originalReport.Empty())
                        continue;

                    Excel.Series dataSeries = chart.SeriesCollection().NewSeries();
                    dataSeries.XValues = data.GetXValues();
                    dataSeries.Values = series.Value;

                    int seriesOrdinalNumber = chart.SeriesCollection().Count() - 1;
                    int seriesColor = xlConfig.chartSeriesColors[seriesOrdinalNumber % xlConfig.chartSeriesColors.Length];

                    // Если у серии отсутствует название
                    string reportSpec = originalReport.ExtrctReportSpec();
                    if (reportSpec.Trim().Length == 0)
                        reportSpec = originalReport.GetMeasureUnit();

                    // Визуальное оформление серии
                    dataSeries.Format.Line.Weight = 2;
                    dataSeries.Format.Line.ForeColor.RGB = seriesColor;
                    dataSeries.Name = (reportSpec == null || reportSpec.Trim().Length == 0) ? "?" : reportSpec.NeatSplit(20);

                    // Специфичные для типа диаграммы стили серии
                    switch (data.GetXlChartType())
                    {
                        case Excel.XlChartType.xlLineMarkers:
                            dataSeries.MarkerBackgroundColor = seriesColor;
                            dataSeries.MarkerSize = 7;
                            dataSeries.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleSquare;
                            break;
                        case Excel.XlChartType.xlColumnClustered:
                            dataSeries.HasDataLabels = true;
                            break;
                    };
                }
            }


            // Добавление серии стандартной метрики SLA в группу
            private void _FillChartWithSlaLines(Excel.Chart chart, xlChartData data)
            {
                int slaId = 0;
                foreach (Excel.Range slaLine in data.GetSlaLines())
                {
                    Excel.Series slaSeries = chart.SeriesCollection().NewSeries();
                    slaSeries.Values = slaLine;
                    slaSeries.Format.Line.Weight = 3;

                    double slaVal = -1;
                    if (slaLine.Rows.Count > 0 && slaLine.Columns.Count > 0 && slaLine.Cells[1, 1].Value is double)
                        slaVal = slaLine.Cells[1, 1].Value;

                    // Визуальное оформление серии
                    slaSeries.Format.Line.ForeColor.RGB = xlConfig.chartSeriesSlaColor;
                    slaSeries.Name = "SLA" +
                        (data.GetSlaLines().Count() == 1 ? "" : ++slaId + "") +
                        (slaVal >= 0 ? " (" + slaVal + ")" : "");
                    slaSeries.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleNone;
                }
            }


            // Добавление заголовка на страницу
            private void _InsertPageHeader(int YCell)
            {
                int totalPageWidthPx =
                    xlConfig.chartWidthPx;
                int totalPageWidthCells =
                    totalPageWidthPx / xlConfig.xlCellWidth;

                Stopwatch w = new Stopwatch(); w.Start();
                // Заголовок листа
                int headerHeightCells = xlConfig.headerHeightPx / xlConfig.xlCellHeight;
                Excel.Range headerStart = (Excel.Range)xlSheetCharts.Cells[YCell, 1];
                Excel.Range headerEnd = (Excel.Range)xlSheetCharts.Cells[YCell + headerHeightCells - 1, totalPageWidthCells];
                Excel.Range header = xlSheetCharts.get_Range(headerStart, headerEnd);
                header.Merge();
                // Заливка  
                header.Interior.Pattern = Excel.XlPattern.xlPatternLinearGradient;
                header.Interior.Gradient.Degree = 0;
                header.Interior.Gradient.ColorStops.Clear();
                header.Interior.Gradient.ColorStops.Add(0).Color = xlConfig.decorHeaderBackgroundStart;
                header.Interior.Gradient.ColorStops.Add(1).Color = xlConfig.decorHeaderBackgroundEnd;
                // Текст
                header.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                header.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                header.Value = "  " + slaDoc.GetConfig().Caption;
                header.Font.Size = 22;
                header.Font.Color = xlConfig.decorHeaderFontColor;


                // Логотип ВТБ в районе заголовка XLS-страниц
                try
                {
                    string logoFile = Path.Combine(Program.GetAppPath(), "vtbslalogo.png");
                    Resources.vtb.Save(logoFile);

                    int logoX = totalPageWidthPx - xlConfig.vtbLogoWidthPx - xlConfig.vtbLogoXRightOffsetpx;
                    int logoY = (YCell-1) * xlConfig.xlCellHeight + (xlConfig.headerHeightPx - xlConfig.vtbLogoHeightPx) / 2;
                    Excel.Shape pic = xlSheetCharts.Shapes.AddPicture(
                        logoFile, MsoTriState.msoFalse, MsoTriState.msoCTrue,
                        logoX, logoY,
                        xlConfig.vtbLogoWidthPx, xlConfig.vtbLogoHeightPx
                        );
                    //pic.PictureFormat.TransparentBackground = MsoTriState.msoCTrue;
                    //pic.PictureFormat.TransparencyColor = xlConfig.chartBackgroundColor;

                    File.Delete(logoFile);
                }
                catch (Exception)
                {
                    // Произошла ошибка при сохранении логотипа ВТБ из ресурсов приложения
                    // в папку, которую указал юзер для сохранения XLS
                    // На этом этапе ошибка не выдается, но скорее всего также не удастся
                    // сохранить и сам XLS. Тогда уже вывалится трабла. В худшем случае - просто не будет лого в XLS.
                }

            }


            // Добавление футера на страницу
            private void _InsertPageFooter(int pageNumber, int YCell)
            {
                int totalPageWidthPx =
                    xlConfig.chartWidthPx;
                int totalPageWidthCells =
                    totalPageWidthPx / xlConfig.xlCellWidth;

                // Заголовок листа
                int footerHeightCells = xlConfig.footerHeightPx / xlConfig.xlCellHeight;
                Excel.Range footerStart = (Excel.Range)xlSheetCharts.Cells[YCell, 1];
                Excel.Range footerEnd = (Excel.Range)xlSheetCharts.Cells[YCell + footerHeightCells - 1, totalPageWidthCells];
                Excel.Range footer = xlSheetCharts.get_Range(footerStart, footerEnd);
                footer.Merge();
                // Заливка  
                footer.Interior.Pattern = Excel.XlPattern.xlPatternLinearGradient;
                footer.Interior.Gradient.Degree = 0;
                footer.Interior.Gradient.ColorStops.Clear();
                footer.Interior.Gradient.ColorStops.Add(0).Color = xlConfig.decorFooterBackgroundStart;
                footer.Interior.Gradient.ColorStops.Add(1).Color = xlConfig.decorFooterBackgroundEnd;
                // Текст
                footer.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                footer.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                footer.Value = pageNumber + "\r\r\r";
                footer.Font.Size = 22;
                footer.Font.Color = xlConfig.decorFooterBackgroundStart;
            }


            // Расчет количества графиков, которые могут поместиться на печатаемую страницу A4
            private int _CalcChartsNumberOnPrintablePage()
            {
                int headerPlusFooterHeightPx = xlConfig.headerHeightPx + xlConfig.footerHeightPx;
                int leftSpacePx = xlConfig.pageA4MaxRows * xlConfig.xlCellHeight - headerPlusFooterHeightPx;
                int chartNumber = leftSpacePx / xlConfig.chartHeightPx;
                int betweenSpaceHeightPx = (leftSpacePx - chartNumber * xlConfig.chartHeightPx) / (2 + chartNumber - 1);

                if (betweenSpaceHeightPx < xlConfig.chartsMinBetweenOffsetPx)
                    chartNumber--;

                return chartNumber;
            }


            // Сохранение в файл             
            private void _SaveToFile()
            {
                try
                {
                    xlWorkBook.SaveAs(outFileName, Excel.XlFileFormat.xlWorkbookNormal, xlNULL, xlNULL, xlNULL, xlNULL,
                        Excel.XlSaveAsAccessMode.xlExclusive, xlNULL, xlNULL, xlNULL, xlNULL, xlNULL);
                }
                catch (Exception)
                {
                    throw new SlaHandledException(ErrorCode.XlsWriteFileNoAccess);
                }
            }

        }
    }
}