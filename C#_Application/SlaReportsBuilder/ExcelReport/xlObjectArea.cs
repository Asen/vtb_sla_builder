﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;

namespace SlaReportsBuilder.ExcelReport
{
    public partial class ExcelBuilder
    {
        private class xlObjectArea
        {
            private Excel.Range objectArea;

            public xlObjectArea(Excel.Worksheet sheet, int cellX, int cellY)
            {
                int cellRight = cellX + xlConfig.chartWidthPx / xlConfig.xlCellWidth - 1;
                int cellBottom = cellY + xlConfig.chartHeightPx / xlConfig.xlCellHeight - 1;

                objectArea = sheet.get_Range(
                        (Excel.Range)sheet.Cells[cellY, cellX],
                        (Excel.Range)sheet.Cells[cellBottom, cellRight]);
            }

            public void ReplaceTo(xlObjectArea area)
            {
                objectArea.Cut(area.objectArea);
            }
        }
    }
}
